package org.communiquons.vnctohttp;

import com.glavsoft.core.SettingsChangedEvent;
import com.glavsoft.drawing.Renderer;
import com.glavsoft.rfb.ClipboardController;
import com.glavsoft.rfb.IRepaintController;
import com.glavsoft.rfb.IRfbSessionListener;
import com.glavsoft.rfb.encoding.PixelFormat;
import com.glavsoft.rfb.encoding.decoder.FramebufferUpdateRectangle;
import com.glavsoft.rfb.protocol.Protocol;
import com.glavsoft.rfb.protocol.ProtocolSettings;
import com.glavsoft.transport.BaudrateMeter;
import com.glavsoft.transport.Transport;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.Socket;

public class VncToHttp implements IRfbSessionListener, IRepaintController, ClipboardController {

    public static void main(String[] args) {
        try {
            if (args.length != 4) {
                System.err.println("Usage: java -jar vnc-to-http.jar [vnc-addr] [vnc-port] [listen-addr] [listen-port]");
                System.exit(1);
            }

            String vncAddr = args[0];
            int vncPort = Integer.parseInt(args[1]);
            String listenAddr = args[2];
            int listenPort = Integer.parseInt(args[3]);


            System.out.printf("Connecting to %s:%d", vncAddr, vncPort);
            Socket socket = new Socket(vncAddr, vncPort);

            new VncToHttp(socket, listenAddr, listenPort);
        } catch (Exception e) {
            System.err.println("Failed to initialize!");
            e.printStackTrace();
        }
    }

    private Protocol workingProtocol;
    private volatile RendererImpl renderer;
    private BufferedImage bufferedImage;
    private Graphics graphics;
    private HttpServer httpServer;

    private VncToHttp(Socket socket, String listenAddr, int listenPort) throws Exception {

        httpServer = new HttpServer(listenAddr, listenPort);

        Transport transport = new Transport(socket);
        final BaudrateMeter baudrateMeter = new BaudrateMeter();
        transport.setBaudrateMeter(baudrateMeter);

        ProtocolSettings protocolSettings = ProtocolSettings.getDefaultSettings();
        protocolSettings.setSharedFlag(true);
        protocolSettings.setViewOnly(true);

        workingProtocol = new Protocol(transport, () -> "PASSWORD", protocolSettings);
        workingProtocol.handshake();

        workingProtocol.startNormalHandling(this, this, this);
    }

    @Override
    public void rfbSessionStopped(String reason) {
        workingProtocol.cleanUpSession();
        System.out.println("VNC session closed");
        System.exit(0);
    }

    @Override
    public void updateSystemClipboard(byte[] bytes) {
    }

    @Override
    public String getRenewedClipboardText() {
        return null;
    }

    @Override
    public String getClipboardText() {
        return "";
    }

    @Override
    public void setEnabled(boolean enable) {

    }

    @Override
    public void settingsChanged(SettingsChangedEvent event) {

    }

    @Override
    public void repaintBitmap(FramebufferUpdateRectangle rect) {
        //System.out.println("repaint bitmap 1 width: " +rect.width +" height: "+ rect.height);
        repaintBitmap(rect.x, rect.y, rect.width, rect.height);
    }

    @Override
    public void repaintBitmap(int x, int y, int width, int height) {
        if (renderer == null || bufferedImage == null)
            return;

        try {
            renderer.paintImageOn(graphics);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "png", outputStream);
            httpServer.setImageBytes(outputStream.toByteArray());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void repaintCursor() {
        // Cursor not supported
        // System.out.println("repaint cursor");
    }

    @Override
    public void updateCursorPosition(short x, short y) {
        System.out.println("change cursor position");
    }

    @Override
    public Renderer createRenderer(Transport transport, int width, int height, PixelFormat pixelFormat) {
        if (graphics != null)
            graphics.dispose();

        renderer = new RendererImpl(transport, width, height, pixelFormat);
        bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        graphics = bufferedImage.createGraphics();
        return renderer;
    }

    @Override
    public void setPixelFormat(PixelFormat pixelFormat) {
        System.out.println("Set pixel format");
        if (renderer != null) {
            renderer.initColorDecoder(pixelFormat);
        }
    }

}
