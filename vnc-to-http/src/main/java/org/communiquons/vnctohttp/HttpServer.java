package org.communiquons.vnctohttp;

import fi.iki.elonen.NanoHTTPD;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

public class HttpServer extends NanoHTTPD {
    private ReentrantLock imageBytesLock = new ReentrantLock();
    private byte[] imageBytes;

    public HttpServer(String addr, int port) throws IOException {
        super(addr, port);
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        System.out.println("Started HTTP server on http://" + addr + ":" + port + "/");
    }

    public void setImageBytes(byte[] imageBytes) {
        imageBytesLock.lock();
        this.imageBytes = imageBytes;
        imageBytesLock.unlock();
    }

    @Override
    public Response serve(IHTTPSession session) {

        if (session.getUri().equals("/"))
            return serveHome();

        if (session.getUri().equals("/image.png"))
            return serveImage(session);

        return super.serve(session);
    }

    private Response serveHome() {
        try {
            byte[] b = HttpServer.class.getResourceAsStream("index.html").readAllBytes();
            return newFixedLengthResponse(Response.Status.OK, "text/html", new String(b));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Response serveImage(IHTTPSession session) {
        try {
            imageBytesLock.lock();

            if (imageBytesLock == null)
                return super.serve(session);


            return newFixedLengthResponse(Response.Status.OK, "image/png", new ByteArrayInputStream(imageBytes), imageBytes.length);
        } finally {
            imageBytesLock.unlock();
        }
    }
}
