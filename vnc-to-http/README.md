# VNC2HTTP
Serve on a regular basis PNG captures of a VNC stream

## Service
Located in: `/etc/systemd/system/vnc-to-http.service`

```
[Unit]
Description=VNC to HTTP service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=pi
ExecStart=/usr/bin/java -jar /home/pi/vnc-to-http-1.1-uber.jar localhost 2564 0.0.0.0 2565

[Install]
WantedBy=multi-user.target
```