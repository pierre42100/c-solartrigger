import urllib.request
import urllib.parse
import hashlib

def fetch_curr_prod(url, fallback):
	'''
	Fetch current solar production

	* url: The URL where to fetch production
	* fallback: The value to use in case of error
	'''

	try:
		
		response = urllib.request.urlopen(url).read().decode("utf-8")
		val = int(response.split("P_Grid")[1].split(":")[1].split(".")[0].replace(" ", ""))
		return val

	except Exception as e:
		print(e)
		return fallback
	

if __name__ == '__main__':
	print("Current production: {}".format(fetch_curr_prod("http://localhost:8088/", -1)))