import RPi.GPIO as GPIO

from time import sleep

GPIO.setmode(GPIO.BOARD)
GPIO.setup(40, GPIO.OUT)

sleep(2)

GPIO.output(40, 1)

sleep(2);

GPIO.output(40,0)

sleep(2)

GPIO.cleanup()
