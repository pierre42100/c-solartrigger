from time import sleep, time

import RPi.GPIO as GPIO

import urllib.request
import urllib.parse
import hashlib

import threading
from http.server import HTTPServer, BaseHTTPRequestHandler

import logging

from conf import *
from date_svc import getHour

STATUS = []
SINCE = []
TOTAL_RUNTIME = []
CURR_PROD = DEFAULT_PROD

# Initialize status
for i in LEVELS:
	STATUS.append(False)
	SINCE.append(int(time()))
	TOTAL_RUNTIME.append(0)

# Initialize GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(ROOT_PIN, GPIO.OUT)
for i in LEVELS_PINS:
	GPIO.setup(i, GPIO.OUT)


count = 0
def get_curr_prod(url, fallback):
	'''
	Fetch current solar production

	* url: The URL where to fetch production
	* fallback: The value to use in case of error
	'''

	try:
		
		response = urllib.request.urlopen(url).read().decode("utf-8")
		val = int(response.split("P_Grid")[1].split(":")[1].split(".")[0].replace(" ", ""))
		return val

	except Exception as e:
		print(e)
		return fallback

def get_curr_hour():
	'''
	Get current hour of the day
	'''
	return getHour(DATE_URL, DEFAULT_HOUR)

last_status = {}

def set_status(pin, status):
	'''
	Set GPIO status
	'''

	# Add the pin if required
	if not pin in last_status:
		last_status[pin] = not status
		print("Saved state of pin {}".format(pin))

	# Update GPIO status if required
	if not status == last_status[pin]:
		GPIO.output(pin, status)
		last_status[pin] = status
	else:
		print("Skipped pin {}".format(pin))


def refresh_gpios():

	if STATUS[MINIMAL_LEVEL_FOR_ROOT_RELAY] == False:
		print("Root relay to off")
		set_status(ROOT_PIN, not False)
	else:
		print("Root relay to on")
		set_status(ROOT_PIN, not True)

	for i in range(0, len(LEVELS)):
		pin = LEVELS_PINS[i]
		if not STATUS[i]:
			print("Relay {} to off on pin {} (have been up for {})".format(i, pin, TOTAL_RUNTIME[i]))
			set_status(pin, not False)
		else:
			print("Relay {} to on on pin {} (have been up for {})".format(i, pin, TOTAL_RUNTIME[i]))
			set_status(pin, not True)


# Simple request handler
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

	def send_404(self):
		# Response
		self.send_response(404)
		self.end_headers()
		self.wfile.write(b"Ressource not found")

	def send_500(self):
		# Response
		self.send_response(500)
		self.end_headers()
		self.wfile.write(b"Internal server error")


	def send_200(self, content):
		# Response
		self.send_response(200)
		self.end_headers()
		self.wfile.write(content.encode("utf-8"))



	def do_GET(self):

		try:
			
			if not "/api/status/" in self.path:
				self.send_404()
				return

			levelID = int(self.path.split("/api/status/")[1])

			if levelID < 0 or levelID >= len(STATUS):
				self.send_404()
				return

			self.send_200("{{\"is_on\": {}, \"for\": {}, \"prod\": {}, \"total_uptime\": {}, \"required_uptime\": {}}}".format(
				"true" if STATUS[levelID] else "false",
				int(time()) - SINCE[levelID],
				CURR_PROD,
				TOTAL_RUNTIME[levelID],
				MINIMAL_RUNTIME[levelID]
			))


		except Exception as e:
			print(e)
			try:
				self.send_500()
			except Exception as e:
				print(e)
			



		


# Start builtin server
def start_server():
	print("Starting server on http://{}:{}/".format(LISTEN_ADDR, LISTEN_PORT))

	# Create the socket & listen to it
	httpd = HTTPServer((LISTEN_ADDR, LISTEN_PORT), SimpleHTTPRequestHandler)
	httpd.serve_forever()

srvThread = threading.Thread(target=start_server)
srvThread.start()




# Main loop
refresh_gpios()
ok_since = -1
bad_since = -1
while True:
	try:
		
		currHour = get_curr_hour()

		# Reset runtimes if required
		if currHour == RESET_HOUR:
			for i, val in enumerate(TOTAL_RUNTIME):
				TOTAL_RUNTIME[i] = 0


		prod = get_curr_prod(URL, DEFAULT_PROD)
		CURR_PROD = prod
		print("Curr prod: {}".format(prod))

		# Stop highest relay if production is bad
		if prod > BAD_PROD_THRESOLD:

			# It is not good anymore
			ok_since = -1

			# We wait a little before shutting down the relays
			if bad_since < 0:
				bad_since = int(time())

			# Check for minimum bad time
			if bad_since + CHECK_TIME < int(time()):

				for i in range(0, len(LEVELS)):
					if STATUS[len(LEVELS)-1-i] == True:

						levelID = len(LEVELS)-1-i

						# We check if we have the minimal uptime
						if SINCE[levelID] + MINIMAL_UPTIME > int(time()):
							break

						# Set to off
						STATUS[levelID] = False
						TOTAL_RUNTIME[levelID] += int(time()) - SINCE[levelID]
						SINCE[levelID] = int(time())
						bad_since = -1
						break

		# Start lowest relay (if possible)
		else:

			# It is not bad anymore
			bad_since = -1

			for i in range(0, len(LEVELS)):
				if not STATUS[i]:

					# We check if we have the minimal downtime
					if SINCE[i] + MINIMAL_DOWNTIME > int(time()):
						break

					# If production is good enough
					if prod + PROD_MARGIN < LEVELS[i]:

						# We check if the production has not been good for enough time
						if ok_since < 0:
							ok_since = int(time())

						# Else we can start the relay
						elif ok_since + CHECK_TIME < int(time()):
							STATUS[i] = True
							SINCE[i] = int(time())
							ok_since = -1

					# If the production is not good enough anymore, reset counter
					else:
						ok_since = -1

					break


		# Check if we are in an hour where we have to fix minimal runtime
		if currHour in FIX_RUNTIME_HOURS:
			for i, status in enumerate(STATUS):
				
				# We ignore levels which are currently on
				if status:
					continue

				if TOTAL_RUNTIME[i] < MINIMAL_RUNTIME[i]:
					STATUS[i] = True
					SINCE[i] = int(time())



		refresh_gpios()

	except Exception as e:
		logging.exception("Main loop failed!")

	sleep(REFRESH_INTERVAL)
