# Levels at which we can go up of one level
LEVELS = [-3300, # <= chauffe eau
		 -2000, -2000, -2000] # <= piscine
PROD_MARGIN = 500

ROOT_PIN = 22
LEVELS_PINS = [26, 4, 18, 17]
 
# Minimal level to start root relay (we need it for swimming pool)
MINIMAL_LEVEL_FOR_ROOT_RELAY = 1

# Minimal runtime per day, in seconds
MINIMAL_RUNTIME = [4*3600, 0, 0, 0]

# Hours at which we force to fix minimal runtime
FIX_RUNTIME_HOURS = [22, 23] # , 0, 1, 2, 3, 4, 5]

# The hour at which we reset minimal runtime
RESET_HOUR = 6

# Refresh interval, in seconds
REFRESH_INTERVAL = 10
CHECK_TIME = 60

MINIMAL_UPTIME=60*5
MINIMAL_DOWNTIME=60*2

# Invalid production thresold
BAD_PROD_THRESOLD = -150

# Fetch URL
URL="http://192.168.2.247/solar_api/v1/GetPowerFlowRealtimeData.fcgi"
DEFAULT_PROD=1 # Fallback production


# Date fetch URL
DATE_URL = "http://192.168.2.64/force_bad_url_for_new_date"
DEFAULT_HOUR=15

# Server info
LISTEN_ADDR = "0.0.0.0"
LISTEN_PORT = 8652
