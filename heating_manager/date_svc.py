import socket
from time import time, sleep

LAST_DATE_FETCH = -1
LAST_DATE = -1
KEEP_DATE_FOR = 600


def fetchDate(url):

	global LAST_DATE
	global LAST_DATE_FETCH

	'''
	Fetch & store remote date of an HTTP response
	'''

	# Extract information about the request
	try:
		domain = url.split("http://")[1].split("/")[0]
		uri = url.split(domain)[1]
		port = 80

		if ":" in domain:
			port = int(domain.split(":")[1])
			domain = domain.split(":")[0]

		# Send the request on the server
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((domain, port))

		req = "GET {} HTTP/1.1\r\nContent-Length: 0\r\nHost: {}\r\n\r\n".format(uri, domain)
		s.send(req.encode("utf-8"))

		response = s.recv(8000).decode("utf-8")

		if not "Date:" in response:
			return False


		# Extract date (ex: 11:17:44)
		date = response.split("Date: ")[1].split(" ")[4]

		# Turn the date into an amount of seconds & store it
		LAST_DATE = int(date[:2])*3600+int(date[3:5])*60+int(date[6:8])
		LAST_DATE_FETCH = int(time())

		s.close()

		return True

	except Exception as e:
		print(e)
		return False

def getHour(url, fallback):
	'''
	Get current hour

	:url: Requested URL
	:fallback: Default value to use if the date could not be extracted
	'''

	# Check if we have already fetched the date within the minute
	if LAST_DATE_FETCH + KEEP_DATE_FOR < int(time()):
		print("Attempt to fetch the date")
		
		if not fetchDate(url):
			print("Could not fetch date!")
			return fallback


	return ((LAST_DATE+int(time())-LAST_DATE_FETCH)//3600) % 24

if __name__ == '__main__':
	for i in range(0, 360):
		print(getHour("http://192.168.1.21:80/fakepath", 16))
		sleep(1)
