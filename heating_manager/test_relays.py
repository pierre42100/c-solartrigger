import RPi.GPIO as GPIO
from time import sleep

from conf import *


# Add root pin to the list of pins to test
LEVELS_PINS.append(ROOT_PIN)

GPIO.setmode(GPIO.BCM)


for i in LEVELS_PINS:
        GPIO.setup(i, GPIO.OUT)


while True:
	for i in LEVELS_PINS:
		GPIO.output(i, False)
		print("Set pin {} to on".format(i))
		sleep(1)

	for i in LEVELS_PINS:
		GPIO.output(i, True)
		print("Set pin {} to off".format(i))
		sleep(1)
