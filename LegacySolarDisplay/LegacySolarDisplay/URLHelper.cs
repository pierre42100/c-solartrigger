using System;
using System.Collections.Generic;
using System.Text;

namespace LegacySolarDisplay
{
    /**
     * URL Helper
     * 
     * This class contains the method related
     * with URL request
     * 
     * @author Pierre HUBERT
     */
    class URLHelper
    {
        /**
         * Get the current URL
         */
        public static string GetCurrentURL()
        {
            return Properties.Settings.Default.ServerURL;
        }

        /**
         * Check if an URL is set yet or not
         */
        public static bool HasURL()
        {
            return GetCurrentURL().Length > 0;
        }

        /**
         * Set new URL
         */
        public static void SetURL(String newURL)
        {
            Properties.Settings.Default.ServerURL = newURL;
            Properties.Settings.Default.Save();
        }

        /**
         * Get current URL information in a parsed format
         */
        public static URLInfo GetURLInfo()
        {
            return new URLInfo(GetCurrentURL());
        }
    }
}
