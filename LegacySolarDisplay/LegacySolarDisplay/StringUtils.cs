using System;
using System.Collections.Generic;
using System.Text;

namespace LegacySolarDisplay
{
    class StringUtils
    {
        public static String[] Split(string str, string delimiter)
        {
            List<String> delims = new List<string>();
            delims.Add(delimiter);
            return str.Split(delims.ToArray(), StringSplitOptions.None);
        }
    }
}
