using System;
using System.Collections.Generic;
using System.Text;

namespace LegacySolarDisplay
{
    /**
     * This class contains a snapshot of production
     * information
     */
    class ProductionInfo
    {
        private int solar;
        private int edf;

        public ProductionInfo(int solar, int edf)
        {
            this.solar = solar;
            this.edf = edf;
        }

        public int Solar
        {
            get { return this.solar; }
        }

        public int Edf
        {
            get { return this.edf; }
        }
    }
}
