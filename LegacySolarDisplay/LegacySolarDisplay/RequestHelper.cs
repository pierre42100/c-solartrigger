using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace LegacySolarDisplay
{
    class RequestHelper
    {
        public static ProductionInfo getCurrentProduction()
        {
            URLInfo info = URLHelper.GetURLInfo();

            string response = ExecHTTPRequest(info.Host, info.Port, info.Uri);

            if (!response.StartsWith("HTTP/1.1 200 OK"))
                throw new Exception("Request failed! - " + response);

            // Parse the response
            return new ProductionInfo(
                ExtractInt(response, "P_PV"),
                ExtractInt(response, "P_Grid")
            );
        }

        /**
         * Extract a value from the response of the server
         */
        private static int ExtractInt(string response, string key)
        {
            key = "\"" + key + "\"";
            string val = StringUtils.Split(response, key)[1];
            val = StringUtils.Split(val, ":")[1];
            
            // Suppress all spaces
            val = val.Replace(" ", "");
            
            if (val.Contains("."))
                val = StringUtils.Split(val, ".")[0];

            val = StringUtils.Split(val, ",")[0];

            return Convert.ToInt32(val);
        }

        /**
         * Create socket connection
         */
        private static Socket ConnectSocket(string server, int port) {
            Socket s = null;
            IPHostEntry hostEntry = null;

            // Get host related information
            hostEntry = Dns.GetHostEntry(server);

            // Search for compatible IP address
            foreach (IPAddress address in hostEntry.AddressList)
            {
                IPEndPoint ipe = new IPEndPoint(address, port);
                Socket tempSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                tempSocket.Connect(ipe);

                if (tempSocket.Connected)
                {
                    s = tempSocket;
                    break;
                }
                else
                {
                    continue;
                }
            }

            return s;
        }


        // This method requests the home page content for the specified server.
        private static string ExecHTTPRequest(string server, int port, string uri)
        {
            string request = "GET "+uri+" HTTP/1.1\r\nHost: " + server +
                "\r\nConnection: Close\r\n\r\n";
            Byte[] bytesSent = Encoding.ASCII.GetBytes(request);
            Byte[] bytesReceived = new Byte[2048];

            // Create a socket connection with the specified server and port.
            Socket s = ConnectSocket(server, port);

            if (s == null)
                return ("Connection failed");

            // Send request to the server.
            s.Send(bytesSent, bytesSent.Length, 0);

            // Receive the server home page content.
            int bytes = 0;
            string page = "";

            // The following will block until the page is transmitted.
            do
            {
                bytes = s.Receive(bytesReceived, bytesReceived.Length, 0);
                page += Encoding.ASCII.GetString(bytesReceived, 0, bytes);
            }
            while (bytes > 0);

            return page;
        }

    }
}
