using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LegacySolarDisplay
{
    public partial class Form1 : Form
    {
        private Timer timer;

        public Form1()
        {
            InitializeComponent();

            // Set initial server URL
            this.serverURLInput.Text = URLHelper.GetCurrentURL();

            // Refresh interval
            InitAutoRefresh();
        }

        private void applySettingsButton_Click(object sender, EventArgs e)
        {
            URLHelper.SetURL(this.serverURLInput.Text);
            System.Windows.Forms.MessageBox.Show(
                "Settings updated.",
                "Success",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information
            );
        }

        private void InitAutoRefresh()
        {
            timer = new Timer();
            timer.Interval = 3000; // Refresh every 3 seconds
            timer.Tick += DidTick;
            timer.Enabled = true;
        }

        private void DidTick(object source, EventArgs ev)
        {
            if (!URLHelper.HasURL())
                return;

            timer.Stop();

            try
            {
                ProductionInfo prod = RequestHelper.getCurrentProduction();

                ApplyProduction(prod);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                ApplyProduction(new ProductionInfo(-1, -1));

                MessageBox.Show(
                    e.ToString(),
                    "Refresh error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
            }

            timer.Start();
        }

        private void ApplyProduction(ProductionInfo prod)
        {
            this.currSolarLabel.Text = prod.Solar.ToString();
            this.currEDFLabel.Text = prod.Edf.ToString();
        }
    }
}