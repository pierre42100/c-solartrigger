using System;
using System.Collections.Generic;
using System.Text;

namespace LegacySolarDisplay
{
    class URLInfo
    {
        private string host;
        private int port = 80;
        private string uri;

        public URLInfo(string url)
        {
            url = url.Replace("http://", "");

            string[] split = url.Split("/".ToCharArray(), 2, StringSplitOptions.None);

            host = split[0];
            uri = "/" + split[1];


            if (host.Contains(":"))
            {
                string[] hostSplit = host.Split(":".ToCharArray());
                host = hostSplit[0];
                port = Convert.ToInt32(hostSplit[1]);
            }
        }

        public string Host { get { return host; } }

        public int Port { get { return port; } }

        public string Uri { get { return uri; } }
    }
}
