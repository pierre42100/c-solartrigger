namespace LegacySolarDisplay
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.applySettingsButton = new System.Windows.Forms.Button();
            this.serverURLInput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.currSolarLabel = new System.Windows.Forms.Label();
            this.currEDFLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Address";
            // 
            // applySettingsButton
            // 
            this.applySettingsButton.Location = new System.Drawing.Point(238, 13);
            this.applySettingsButton.Name = "applySettingsButton";
            this.applySettingsButton.Size = new System.Drawing.Size(75, 23);
            this.applySettingsButton.TabIndex = 1;
            this.applySettingsButton.Text = "Apply";
            this.applySettingsButton.UseVisualStyleBackColor = true;
            this.applySettingsButton.Click += new System.EventHandler(this.applySettingsButton_Click);
            // 
            // serverURLInput
            // 
            this.serverURLInput.Location = new System.Drawing.Point(63, 13);
            this.serverURLInput.Name = "serverURLInput";
            this.serverURLInput.Size = new System.Drawing.Size(169, 20);
            this.serverURLInput.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(56, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 39);
            this.label2.TabIndex = 3;
            this.label2.Text = "Solar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(56, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 39);
            this.label3.TabIndex = 4;
            this.label3.Text = "EDF";
            // 
            // currSolarLabel
            // 
            this.currSolarLabel.AutoSize = true;
            this.currSolarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currSolarLabel.Location = new System.Drawing.Point(199, 51);
            this.currSolarLabel.Name = "currSolarLabel";
            this.currSolarLabel.Size = new System.Drawing.Size(47, 39);
            this.currSolarLabel.TabIndex = 5;
            this.currSolarLabel.Text = "-1";
            // 
            // currEDFLabel
            // 
            this.currEDFLabel.AutoSize = true;
            this.currEDFLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currEDFLabel.Location = new System.Drawing.Point(199, 104);
            this.currEDFLabel.Name = "currEDFLabel";
            this.currEDFLabel.Size = new System.Drawing.Size(47, 39);
            this.currEDFLabel.TabIndex = 6;
            this.currEDFLabel.Text = "-1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 160);
            this.Controls.Add(this.currEDFLabel);
            this.Controls.Add(this.currSolarLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.serverURLInput);
            this.Controls.Add(this.applySettingsButton);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Legacy Solar Display";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button applySettingsButton;
        private System.Windows.Forms.TextBox serverURLInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label currSolarLabel;
        private System.Windows.Forms.Label currEDFLabel;
    }
}

