'''
GPS time fetcher

This programm must be run as root

@author Pierre HUBERT
'''

import datetime


GPS_PATH="/dev/ttyACM0"
TIME_SHIFT=2

def get_gps_time():

	f = open(GPS_PATH, "r")

	while True:
		res = f.readline()
		
		if not "GGA" in res:
			continue

		# Check if we do not have time fix yet
		if "$GNGGA,," in res:
			continue

		# Turn date into object
		time = res.split("$GNGGA,")[1].split(".")[0]

		# Check if time size is invalid
		if len(time) != 6:
			continue

		hours = int(time[:2]) + TIME_SHIFT
		mins = int(time[2:4])
		secs = int(time[4:])
		
		return datetime.time(hours, mins, secs)
	


if __name__ == '__main__':
	print(get_gps_time())