export NUM_LEVELS=1
export LEVEL_1=-2000
export PIN_1=4

if [[ "$1" == "test" ]]
then
	python3 test_relays.py
else
	python3 service.py
fi
