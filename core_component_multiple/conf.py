import os

def conf(key, default):
	val = os.environ.get(key)
	return default if val is None else val

# production thresolds
NUM_LEVELS = int(conf("NUM_LEVELS", "2"))
LEVELS = []
LEVELS_PINS = []


# Margin of overproduction before triggering relay
PROD_MARGIN = int(conf("PROD_MARGIN", "500"))

default_levels = [4, 17]

for level in range(NUM_LEVELS):
	# LEVEL_0, LEVEL_1...
	LEVELS.append(int(conf(f"LEVEL_{level}", "-2000")))

	# PIN_0, PIN_1...
	LEVELS_PINS.append(int(conf(f"PIN_{level}", str(default_levels[level]))))


# Refresh interval, in seconds
REFRESH_INTERVAL = int(conf("REFRESH_INTERVAL", "10"))
CHECK_TIME = int(conf("CHECK_TIME", "10"))

MINIMAL_UPTIME=int(conf("MINIMAL_UPTIME", "10"))
MINIMAL_DOWNTIME=int(conf("MINIMAL_DOWNTIME", "10"))

# Invalid production thresold
BAD_PROD_THRESOLD = int(conf("BAD_PROD_THRESOLD", "-150"))

# Fetch URL
URL=conf("DATA_URL", "http://192.168.1.247/solar_api/v1/GetPowerFlowRealtimeData.fcgi")
DEFAULT_PROD=1 # Fallback production


# Server info
LISTEN_ADDR = "0.0.0.0"
LISTEN_PORT = 8652

