# Core component with multiple devices
This client manages several clients at the same time.

## Dependencies
```bash
sudo apt-get install python-rpi.gpio python3-rpi.gpio
```

## Network configuration
Edit the file `/etc/dhcpcd.conf`, uncomment & adapt the following lines:

```
interface eth0
static ip_address=192.168.1.81/24
static routers=192.168.1.1
```

Then reboot the Raspberry Pi to apply changes.


## Installing as service
```sh
sudo cp core_component_multiple.service /etc/systemd/system/
sudo systemctl start core_component_multiple
sudo systemctl enable core_component_multiple
```
