/**
 * Control thread
 *
 * This thread is built to prevent dead lock
 * on pull thread
 *
 * @author Pierre HUBERT
 */

void control_thread_start();

void control_thread_stop();