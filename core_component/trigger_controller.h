/**
 * Trigger controller
 *
 * This file contains the functions that allows to decide to 
 * turn on or off the trigger
 *
 * @author Pierre HUBERT
 */

#pragma once

/**
 * Perform a checkin (has to be called each time
 * the production value is refreshed)
 */
void trigger_controller_checkin();