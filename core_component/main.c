#include "global.h"
#include "pull_thread.h"
#include "notifier.h"
#include "trigger_controller.h"
#include "server_thread.h"
#include "relay_controller.h"
#include "control_thread.h"

int main(int argc, char ** argv){

	LOG_VERBOSE("Solar trigger. (c) Pierre HUBERT 2019. Licensed under the MIT License.");

	LOG_VERBOSE((ON_RASPBERRY ? "On raspberry" : "NOT on raspberry"));

	//Initialize notifier
	notifier_init();

	//Initialize pull thread
	pull_thread_init();

	//Initialize server thread
	server_thread_start();

	// Initialize relay controller
	relay_controller_init();

	// Start control thread
	control_thread_start();

	while(1){
		notifier_wait();

		trigger_controller_checkin();
		
		notifier_release();
	}

	// Stop control thread
	control_thread_stop();


	//Wait for the pull thread to join
	pull_thread_join();

	//Quit pull thread
	pull_thread_quit();

	// Free relay controller
	relay_controller_quit();
}