/**
 * Production update notifier
 *
 * @author Pierre
 */

#pragma once

/**
 * Initialize notifier
 */
void notifier_init();

/**
 * Wait for notifier
 */
void notifier_wait();

/**
 * Must be called in the same thread as notifier_thread
 */
void notifier_release();

/**
 * Notify for update
 */
void notifier_notify();