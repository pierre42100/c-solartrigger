/**
 * Thread dedicated to the HTTP Server associated with the service
 *
 * @author Pierre HUBERT
 */

#pragma once

/**
 * Start the server thread
 */
void server_thread_start();