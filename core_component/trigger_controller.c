#include <time.h>

#include "global.h"
#include "current_status.h"
#include "current_prod.h"
#include "trigger_controller.h"
#include "relay_controller.h"

void trigger_controller_turn_off() {
	current_status_t new_status = {0, time(NULL)};
	current_status_set(&new_status);
	relay_controller_switch(0);
}

void trigger_controller_turn_on() {
	current_status_t new_status = {1, time(NULL)};
	current_status_set(&new_status);
	relay_controller_switch(1);
}

void trigger_controller_checkin() {

	//Get current values
	int curr_prod = current_prod_get();
	current_status_t status = current_status_get();

	//Here comes to logic to check whether to enable or not the relay
	if(status.is_on
		&& curr_prod > PRODUCTION_LIMIT
		&& time(NULL) - MINIMAL_RUNTIME  > status.since){

		trigger_controller_turn_off();

	}

	else if(!status.is_on
		&& curr_prod < PRODUCTION_LIMIT - DEVICE_CONSUMPTION)
		trigger_controller_turn_on();
}