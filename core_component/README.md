# Solar Trigger main component

This is the main software of the project. It is the part of the project that connects to a solar server and retrieve data from it to decides wether to start or not an electrical component.


## Dependencies
```sh
sudo apt install -y libcurl4-openssl-dev
```


## Installing as service
```sh
sudo cp solar_trigger.service /etc/systemd/system/
sudo systemctl start solar_trigger
sudo systemctl enable solar_trigger
```

## Test service
```sh
curl http://[RASPI_URL]:8089/api/status
```

## Test on a computer (disable real GPIO manipulation)
```sh
make all ON_RASPBERRY=0
```
