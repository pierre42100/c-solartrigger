#include <time.h>
#include <pthread.h>

#include "current_prod.h"
#include "config.h"

static int curr_prod = DEFAULT_VALUE;
static int last_update = 0;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void current_prod_set(int value) {
	pthread_mutex_lock(&mutex);
	curr_prod = value;
	last_update = time(NULL);
	pthread_mutex_unlock(&mutex);
}


int current_prod_get() {
	pthread_mutex_lock(&mutex);
	int value = curr_prod;
	pthread_mutex_unlock(&mutex);
	return value;
}

int current_prod_last_update() {
	pthread_mutex_lock(&mutex);
	int value = last_update;
	pthread_mutex_unlock(&mutex);
	return value;
}