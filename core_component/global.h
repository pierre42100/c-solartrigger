/**
 * Global file of the project. Should be included
 * in every file of the project
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"

#define LOG_VERBOSE(msg) printf("VERBOSE: %s\n", msg);
#define LOG_ERROR(msg) {fprintf(stderr, "ERROR: %s\n", msg);}
#define FATAL_ERROR(msg) {fprintf(stderr, "FATAL_ERROR: %s\n", msg); exit(EXIT_FAILURE);}

#define MALLOC(var, size){var = malloc(size); if(var == NULL) FATAL_ERROR("Could not allocate memory!");}

#define STR_TO_ALLOC(var, string) {MALLOC(var, sizeof(char)*(strlen(string) + 1)); strcpy(var, string);}