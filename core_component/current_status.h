/**
 * Current trigger status
 *
 * @author Pierre HUBERT
 */

typedef struct current_status_t {
	int is_on;
	unsigned int since;
} current_status_t;

/**
 * Get current status
 */
current_status_t current_status_get();

/**
 * Set new status
 */
void current_status_set(current_status_t *status);