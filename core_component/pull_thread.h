/**
 * Pull thread
 *
 * This thread is used to retrieve data from the
 * solar inverter
 * 
 * @author Pierre HUBERT
 */

/**
 * Initialize pull thread
 */
void pull_thread_init();

/**
 * Wait for the pull thread to quit
 */
void pull_thread_join();

/**
 * Quit pull thread
 */
void pull_thread_quit();

/**
 * Restart pull thread
 */
void pull_thread_restart();