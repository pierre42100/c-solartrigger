/**
 * Current production getter and setter
 *
 * @author Pierre HUBERT
 */

/**
 * Set new production value
 */
void current_prod_set(int value);

/**
 * Get current production value
 */
int current_prod_get();

/**
 * Get current production last update
 */
int current_prod_last_update();