#include <pthread.h>
#include <unistd.h>
#include <time.h>

#include "global.h"
#include "current_prod.h"
#include "control_thread.h"
#include "pull_thread.h"


static void * controlThread(void*p) {
	(void)p;

	while(1) {
		if(time(NULL) - current_prod_last_update() >= 60) {

			LOG_ERROR("Need to restart pull thread, seems locked...")

			pull_thread_restart();
		}

		sleep(1);		
	}

	return NULL;
}


void control_thread_start() {
	pthread_t thread;
	pthread_create(&thread, NULL, controlThread, NULL);
}

void control_thread_stop() {
	// Not implemented
}