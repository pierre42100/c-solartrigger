#include <pthread.h>
#include <time.h>

#include "global.h"
#include "server_thread.h"
#include "http_server.h"
#include "current_prod.h"
#include "current_status.h"

static pthread_t thread;

/**
 * Get current relay status route
 */
static void GetStatusRoute(const http_server_request_t *req, 
	http_server_response_t *res) {

	//Basic information
	res->code = 200;
	STR_TO_ALLOC(res->content_type, "application/json");


	//Gather information
	int prod = current_prod_get();
	current_status_t status = current_status_get();


	char msg[200];
	sprintf(
		msg, 
		"{\"is_on\": %s,\"for\": %lu,\"prod\": %d}",
		status.is_on ? "true" : "false",
		status.since == 0 ? 0 : time(NULL) - status.since,
		prod
	);

	STR_TO_ALLOC(res->content, msg);
}


static void *RunServer(void *n){

	printf("Start HTTP Server on port %d\n", INTERNAL_SERVER_PORT);

	http_server_t *server = http_server_init();
	http_server_add_route(server, "/api/status", GetStatusRoute);
	http_server_serve(server, INTERNAL_SERVER_PORT, INTERNAL_SERVER_NAME);

	return n;
}


void server_thread_start() {
	if(pthread_create(&thread, NULL, RunServer, NULL) != 0)
		FATAL_ERROR("Could not start server thread!");
}