#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <curl/curl.h>
#include <string.h>

#include "pull_thread.h"
#include "global.h"
#include "current_prod.h"
#include "notifier.h"

static pthread_t pull_thread;

static void * PullThread(void *p);
static int GetCurrentProduction();
static int ExtractIntFromJSON(const char const *, const char const *, int);

void startThread() {
	if(pthread_create(&pull_thread, NULL, PullThread, NULL) < 0)
		FATAL_ERROR("Could not initialize pull thread!");
}

void pull_thread_init() {

	curl_global_init(CURL_GLOBAL_ALL);

	startThread();

	// Set default production
	current_prod_set(DEFAULT_VALUE);
}

void pull_thread_join() {
	pthread_join(pull_thread, NULL);
}

void pull_thread_quit() {
	pthread_kill(pull_thread, 0);

	curl_global_cleanup();
}

void pull_thread_restart() {

	pthread_t thread;
	pull_thread = thread;

	startThread();
}

/**
 * Pull thread loop
 */
static void * PullThread(void *p){
	LOG_VERBOSE("Pull thread started");

	while(1){
		int prod = GetCurrentProduction();
		printf("Current production: %d\n", prod);
		current_prod_set(prod);
		notifier_notify(); //Notify other threads
		usleep(REFRESH_INTERVAL);
	}


	return NULL;
}


struct MemoryStruct {
  char *memory;
  size_t size;
};

static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;

  mem->memory = realloc(mem->memory, mem->size + realsize + 1);
  if(mem->memory == NULL) {
	/* out of memory! */
	printf("not enough memory (realloc returned NULL)\n");
	return 0;
  }

  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;

  return realsize;
}

/**
 * Retrieve current production from server, using cURL
 */
static int GetCurrentProduction(){
	CURL *curl;
	CURLcode res;
	struct MemoryStruct chunk = {NULL, 0};
	int prod = DEFAULT_VALUE;

	curl = curl_easy_init();

	if(!curl)
		FATAL_ERROR("Could not initialize cURL for a request!");

	//Destination URL
	curl_easy_setopt(curl, CURLOPT_URL, SERVER_URL);
	
	/* send all data to this function  */
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

	/* we pass our 'chunk' struct to the callback function */
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);	

	res = curl_easy_perform(curl);

	//Check for errors
	if(res != CURLE_OK)
		fprintf(stderr, "curl_easy_perform() failed: %s\n",
			curl_easy_strerror(res));

	//Process new value
	else {
		//Extract the value
		prod = ExtractIntFromJSON(chunk.memory, "P_Grid", prod);
	}

	//Free allocated memory
	curl_easy_cleanup(curl);
	if(chunk.memory != NULL) free(chunk.memory);

	return prod;
}

static int ExtractIntFromJSON(const char const * content, 
	const char const * entry, int d) {

	char *key;
	int keylenght;
	MALLOC(key, sizeof(char)*(strlen(entry) + 3));
	sprintf(key, "\"%s\"", entry);
	keylenght = strlen(key);

	char *begin = strstr(content, key);
	free(key);

	if(begin == NULL)
		return d;
	char *curr = begin + keylenght;

	while((*curr < '0' || *curr > '9') && *curr != '-' && *curr != '\0')
		curr++;

	if(*curr == '\0')
		return d;

	int negative = 0;
	int value = 0;

	if(*curr == '-'){
		negative = 1;
		curr++;
	}

	while(*curr >= '0' && *curr <= '9'){
		value *= 10;
		value += *curr - '0';
		curr++;
	}

	if(negative)
		value *= -1;

	return value;
}