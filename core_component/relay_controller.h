/**
 * Relay controller
 *
 * Here are implemented the methods that allows the programm
 * to interact with the real world
 *
 * @author Pierre HUBERT
 */

#pragma once

/**
 * Initialize relay controller
 */
void relay_controller_init();

/**
 * Quit relay controller
 */
void relay_controller_quit();

/**
 * Switch relay controller status
 *
 * @param enable 1 = enable / 0 = disable
 */
void relay_controller_switch(int enable);