/**
 * Project configuration
 *
 * @author Pierre HUBERT
 */

#pragma once

/**
 * Server URL
 */
#define SERVER_URL "http://localhost:8088/"

/**
 * The thresold below which one the relay will be triggered
 */
#define PRODUCTION_LIMIT -100

/**
 * Device consumption : the amount of electricity consumed by the desired
 * device
 */
#define DEVICE_CONSUMPTION 50

/**
 * The minimal amount of time the device has to stay on once it has
 * been started
 */
#define MINIMAL_RUNTIME 10

/**
 * Refresh interval (in nanosec)
 */
#define REFRESH_INTERVAL 1000000

/**
 * Value to return in case of failure
 */
#define DEFAULT_VALUE 0

/**
 * Internal sever information
 */
#define INTERNAL_SERVER_NAME "SolarTriggerCore"
#define INTERNAL_SERVER_PORT 8089

/**
 * GPIO port number used to control the relay (BCM numerotation)
 */
#define GPIO_PORT_NUMBER 21