#include <pthread.h>

#include "notifier.h"

/// Notifier structure
struct notifier_t
{
	pthread_mutex_t mutex;
	pthread_cond_t condition;
};

struct notifier_t n = {PTHREAD_MUTEX_INITIALIZER, 
	PTHREAD_COND_INITIALIZER};


void notifier_init() {
	//Nothing yet
}

void notifier_wait() {
	pthread_mutex_lock(&n.mutex);
	pthread_cond_wait(&n.condition, &n.mutex);
}

void notifier_release() {
	pthread_mutex_unlock(&n.mutex);
}

void notifier_notify() {
	pthread_mutex_lock(&n.mutex);
	pthread_cond_signal(&n.condition);
	pthread_mutex_unlock(&n.mutex);
}