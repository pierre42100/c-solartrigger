#include <pthread.h>

#include "current_status.h"
#include "config.h"

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static current_status_t current_status = {0, 0};

current_status_t current_status_get() {
	pthread_mutex_lock(&mutex);
	current_status_t status = current_status;
	pthread_mutex_unlock(&mutex);
	return status;
}

void current_status_set(current_status_t *new_status) {
	pthread_mutex_lock(&mutex);
	current_status = *new_status;
	pthread_mutex_unlock(&mutex);
}