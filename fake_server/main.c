/**
 * Fake fronius server
 *
 * @author Pierre HUBERT
 *
 * P_PV : Solar production
 * P_Grid : EDF consumption
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "http_server.h"

#define SERVER_PORT 8088
#define SERVER_NAME "Fake Fronius Server"

#define FATAL_ERROR(msg) {fprintf(stderr, "Fatal: %s\n", msg); exit(EXIT_FAILURE);}

/**
 * Current production
 */
int curr_prod = 564;


/**
 * Unique route of this project, deliver a false production value
 */
void request_route(const http_server_request_t *request, 
	http_server_response_t *response) {

	response->code = 200;

	char * message = "{\"Body\":{\"Data\":{\"Site\":{\"P_Grid\":%d.545, \"P_PV\": %d}}}}";
	response->content = malloc(sizeof(char)*(strlen(message) + 30));
	sprintf(response->content, message, curr_prod, curr_prod*2);

	char *content_type = "application/json";
	response->content_type = malloc(sizeof(char)*(strlen(content_type) + 1));
	strcpy(response->content_type, content_type);

}

/**
 * Start server
 */
void *serve(void *n) {

	//Start server
	http_server_t *server = http_server_init();
	http_server_add_route(server, "/", &request_route);
	printf("Fake server listen on port %d\n", SERVER_PORT);
	http_server_serve(server, SERVER_PORT, SERVER_NAME);
	http_server_quit(server);

	return NULL;
}



int main(int argc, char **argv) {

	//Start thread
	pthread_t thread;
	if(pthread_create(&thread, NULL, serve, NULL) < 0)
		FATAL_ERROR("Could not start thread for server!");


	//We offer continuously to the user to update the value
	while(1){

		printf("Current value : %d New value : ", curr_prod);
		scanf("%d", &curr_prod);
	}

	pthread_join(thread, NULL);
	return EXIT_SUCCESS;
}