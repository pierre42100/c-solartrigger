use std::process::exit;

use actix::{Actor, ActorContext, StreamHandler, AsyncContext};
use actix::clock::{Instant, Duration};
use actix_web::{App, Error, HttpRequest, HttpResponse, web};
use actix_web::HttpServer;
use actix_web_actors::ws;
use actix_web_actors::ws::{Message, ProtocolError};

struct WsSession {
    hb: Instant,
}

/// How often heartbeat pings are sent
const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
/// How long before lack of client response causes a timeout
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);

impl WsSession {
    /// helper method that sends ping to client every second.
    ///
    /// also this method checks heartbeats from client
    fn hb(&self, ctx: &mut ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            // check client heartbeats
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                // heartbeat timed out
                println!("Websocket Client heartbeat failed, disconnecting!");

                // stop actor
                ctx.stop();

                // don't try to send a ping
                return;
            }

            ctx.ping(b"");
        });
    }
}

impl Actor for WsSession {
    type Context = ws::WebsocketContext<Self>;

    /// Method is called on actor start.
   /// We register ws session with ChatServer
    fn started(&mut self, ctx: &mut Self::Context) {
        // we'll start heartbeat process on session start.
        self.hb(ctx);
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WsSession {
    fn handle(&mut self, item: Result<Message, ProtocolError>, ctx: &mut Self::Context) {
        let msg = match item {
            Err(e) => {
                eprintln!("WS Error! {:#?}", e);
                ctx.stop();
                return;
            }
            Ok(msg) => msg
        };

        println!("WEBSOCKET MESSAGE: {:?}", msg);

        match msg {
            Message::Text(txt) => ctx.text(txt),
            Message::Binary(bin) => ctx.binary(bin),
            Message::Continuation(_) => {}
            Message::Ping(data) => {
                self.hb = Instant::now();
                ctx.pong(&data);
            }
            Message::Pong(_) => {
                self.hb = Instant::now();
            }
            Message::Close(_) => {
                ctx.stop();
            }
            Message::Nop => {}
        }
    }
}

async fn websocket_route(req: HttpRequest,
                         stream: web::Payload) -> Result<HttpResponse, Error> {
    ws::start(
        WsSession {
            hb: Instant::now(),
        },
        &req,
        stream,
    )
}

async fn index_page() -> HttpResponse {
    let bytes = include_bytes!("index.html") as &'static [u8];
    HttpResponse::Ok().body(bytes)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let args = std::env::args().collect::<Vec<String>>();

    if args.len() != 3 {
        eprintln!("Usage: {} [listen-addr] [listen-port]", args[0]);
        exit(1);
    }

    let addr = args[1].to_string();
    let port = args[2].parse::<u64>().unwrap();

    println!("Start Websocket echo service at ws://{}:{}/ws", addr, port);

    HttpServer::new(move || {
        App::new()
            .service(web::resource("/ws").to(websocket_route))
            .route("**", web::get().to(index_page))
    })
        .bind(format!("{}:{}", addr, port))?
        .run()
        .await
}
