#include "configurationhelper.h"
#include "statusimagerefresh.h"

#include <QPixmap>
#include <QNetworkReply>

StatusImageRefresh::StatusImageRefresh(QObject *parent) : QObject(parent)
{

}

void StatusImageRefresh::requestNewImage()
{
    // Perform a request on the server
    QNetworkReply * reply = mAccessManager.get(
                QNetworkRequest(QUrl(
                                    ConfigurationHelper().getConfig().statusImageURL()
                                    )
                                )
                );

    connect(reply, &QNetworkReply::finished, this, &StatusImageRefresh::finished);
}

void StatusImageRefresh::finished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    reply->deleteLater();

    QByteArray data = reply->readAll();

    QPixmap pm;
    if (!pm.loadFromData(data, "PNG"))
        qDebug("Failed to load image!");

    else
        emit newStatusImage(pm);
}
