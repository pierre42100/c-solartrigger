/**
 * Trigger status
 *
 * @author Pierre HUBERT
 */

#include <QString>

#pragma once

enum TriggerStatusEnum {ON, OFF, ERROR};

class TriggerStatus
{
public:
    TriggerStatus();
    TriggerStatus(TriggerStatusEnum status, int since, int progress = -1);

    TriggerStatusEnum status() const;

    int since() const;

    QString sinceStr() const;

    int progress() const;

    bool hasProgress() const;

private:
    TriggerStatusEnum mStatus;
    int mSince;
    int mProgress;
};
