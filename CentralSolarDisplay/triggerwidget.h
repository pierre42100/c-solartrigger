/**
 * Single trigger widget
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QWidget>

#include "triggerconfiguration.h"
#include "triggerstatusrefresh.h"

namespace Ui {
class TriggerWidget;
}

class TriggerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TriggerWidget(const TriggerConfiguration &conf, QWidget *parent = 0);
    ~TriggerWidget();

public slots:
    void refresh();
    void newDataAvailable(const TriggerStatus &status);


protected:
    void mouseDoubleClickEvent(QMouseEvent *) override;


private:
    Ui::TriggerWidget *ui;
    TriggerConfiguration mConfig;
    TriggerStatus mStatus;
    TriggerStatusRefresh *mRefresh;
};
