#include "triggerstatus.h"

TriggerStatus::TriggerStatus()
{
    this->mStatus = TriggerStatusEnum::ERROR;
    this->mSince = 0;
}

TriggerStatus::TriggerStatus(TriggerStatusEnum isOn, int since, int progress)
    : mStatus(isOn), mSince(since), mProgress(progress)
{

}

TriggerStatusEnum TriggerStatus::status() const
{
    return mStatus;
}

int TriggerStatus::since() const
{
    return mSince;
}

QString TriggerStatus::sinceStr() const
{
    int s = mSince % 60;
    int m = (mSince / 60) % 60;
    int h = mSince / 60 / 60;

    return QString::number(h) + " h " +
            QString::number(m) + " m " +
            QString::number(s) + " s";
}

int TriggerStatus::progress() const
{
    return mProgress;
}

bool TriggerStatus::hasProgress() const
{
    return mProgress >= 0;
}
