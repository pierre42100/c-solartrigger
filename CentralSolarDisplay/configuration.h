/**
 * Application configuration
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QString>
#include <QList>

#include "triggerconfiguration.h"

class Configuration
{
public:
    Configuration();

    QString inverterProductionURL() const;
    void setInverterProductionURL(const QString &inverterProductionURL);

    QString triggers() const;
    void setTriggers(const QString &triggers);

    QList<TriggerConfiguration> triggersList() const;

    int refreshFrequency() const;
    void setRefreshFrequency(int refreshFrequency);

    bool framelessWindow() const;
    void setFramelessWindow(bool framelessWindow);

    bool windowStayOnTop() const;
    void setWindowStayOnTop(bool windowStayOnTop);

    bool getShowInverterProduction() const;
    void setShowInverterProduction(bool value);

    bool showStatusImage() const;
    void setShowStatusImage(bool value);

    int statusImageRefreshInterval() const;
    void setStatusImageRefreshInterval(int statusImageRefreshInterval);

    QString statusImageURL() const;
    void setStatusImageURL(const QString &statusImageURL);

    bool getShowTriggers() const;
    void setShowTriggers(bool showTriggers);

    int statusImageHeight() const;
    void setStatusImageHeight(int height);

    bool centerWindow() const;
    void setCenterWindow(bool centerWindow);

    int windowPosX() const;
    void setWindowPosX(int);

    int windowPosY() const;
    void setWindowPosY(int);

    bool showWebView() const;
    void setShowWebView(bool showWebView);

    int webViewWidth() const;
    void setWebViewWidth(int webViewWidth);

    int webViewHeight() const;
    void setWebViewHeight(int webViewHeight);

    QString webViewURL() const;
    void setWebViewURL(const QString &webViewURL);

    QString webViewStyle() const;
    void setWebViewStyle(const QString &webViewStyle);

private:
    bool mShowInverterProduction;
    QString mInverterProductionURL;

    bool mShowTriggers;
    QString mTriggers;

    int mRefreshFrequency;
    bool mFramelessWindow;
    bool mWindowStayOnTop;
    bool mCenterWindow;
    int mWindowPosX;
    int mWindowPosY;


    bool mShowStatusImage;
    int mStatusImageHeight;
    QString mStatusImageURL;
    int mStatusImageRefreshInterval;

    bool mShowWebView;
    int mWebViewWidth;
    int mWebViewHeight;
    QString mWebViewStyle;
    QString mWebViewURL;
};
