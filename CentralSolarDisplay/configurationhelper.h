/**
 * Configuration helper
 *
 * Manipulates project's configuration
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QObject>

#include "configuration.h"

class ConfigurationHelper : public QObject
{
    Q_OBJECT
public:
    explicit ConfigurationHelper(QObject *parent = 0);

    Configuration getConfig();

    void setConfig(const Configuration &conf);

signals:

public slots:
};
