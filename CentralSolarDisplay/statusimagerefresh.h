#pragma once

#include <QNetworkAccessManager>
#include <QObject>
#include <QPixmap>

class StatusImageRefresh : public QObject
{
    Q_OBJECT
public:
    explicit StatusImageRefresh(QObject *parent = nullptr);

    void requestNewImage();

signals:
    void newStatusImage(QPixmap pm);

private slots:
    void finished();

private:
    QNetworkAccessManager mAccessManager;
};

