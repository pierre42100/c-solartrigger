#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settingsdialog.h"
#include "configurationhelper.h"
#include "config.h"
#include "triggerwidget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    const auto conf = ConfigurationHelper().getConfig();

    this->setWindowFlag(Qt::FramelessWindowHint, conf.framelessWindow());
    this->setWindowFlag(Qt::WindowStaysOnTopHint, conf.windowStayOnTop());

    if (!conf.centerWindow())
    {
        // TODO : understand why it refuses to work
        qDebug("Move window to x: %d    y: %d", conf.windowPosX(), conf.windowPosY());
        move(-100, -100);
    }

    mInverterProdRefresh = new InverterProductionRefresh;
    connect(mInverterProdRefresh, &InverterProductionRefresh::gotNewValues,
            this, &MainWindow::newInverterProduction);

    mStatusImageRefresh = new StatusImageRefresh;
    connect(mStatusImageRefresh, &StatusImageRefresh::newStatusImage,
            this, &MainWindow::newStatusImage);

    // Refresh UI
    refreshUI();
}

MainWindow::~MainWindow()
{
    delete ui;
    mInverterProdRefresh->deleteLater();
    mStatusImageRefresh->deleteLater();
}

void MainWindow::refreshTimeout()
{
    if (ConfigurationHelper().getConfig().getShowInverterProduction())
        mInverterProdRefresh->requestRefresh();
}

void MainWindow::refreshStatusImageTimeout()
{
    if (ConfigurationHelper().getConfig().showStatusImage())
        mStatusImageRefresh->requestNewImage();
}

void MainWindow::newInverterProduction(const InverterProduction &prod)
{
    ui->solar_prod->setText(QString::number(prod.solarProduction()));
    ui->edf_prod->setText(QString::number(prod.eDFProduction()));
}

void MainWindow::newStatusImage(const QPixmap pm)
{
    auto pix = pm.scaledToHeight(ConfigurationHelper().getConfig().statusImageHeight());
    ui->statusWidget->setPixmap(pix);
}

void MainWindow::applyWebViewStyles()
{
    Configuration conf = ConfigurationHelper().getConfig();

    QString js = "(function(){"
                 "var el = document.createElement(\"style\");"
                 "el.innerHTML=\"" + conf.webViewStyle().replace("\"", "\\\"").replace("\n", "") + "\";"
                 "document.head.appendChild(el);"
                 "})();";

    webview->page()->runJavaScript(js);
}

void MainWindow::on_actionSettings_triggered()
{
    SettingsDialog().exec();
    refreshUI();
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::refreshUI()
{
    Configuration conf = ConfigurationHelper().getConfig();

    // Main refresh
    if (mMainTimer != nullptr)
        mMainTimer->deleteLater();

    mMainTimer = new QTimer(this);
    mMainTimer->setInterval(conf.refreshFrequency() * 1000);
    connect(mMainTimer, &QTimer::timeout, this, &MainWindow::refreshTimeout);
    mMainTimer->start();

   for(int i = 0; i < ui->invertersProductionLayout->count(); i++) {

       QLayoutItem *obj = ui->invertersProductionLayout->itemAt(i);

        if(obj->widget() == nullptr)
            continue;

        obj->widget()->setVisible(conf.getShowInverterProduction());
    }


    // Status image
    ui->statusWidget->setVisible(conf.showStatusImage());
    if (conf.showStatusImage()) {
        if (mStatusImageMainTimer != nullptr)
            mStatusImageMainTimer->deleteLater();

        ui->statusWidget->setMinimumHeight(conf.statusImageHeight());
        ui->statusWidget->setMaximumHeight(conf.statusImageHeight());

        mStatusImageMainTimer = new QTimer(this);
        mStatusImageMainTimer->setInterval(conf.statusImageRefreshInterval());
        connect(mStatusImageMainTimer, &QTimer::timeout, this, &MainWindow::refreshStatusImageTimeout);
        mStatusImageMainTimer->start();
    }

    // Triggers
    createTriggers(conf);

    // Webview
    if (webview != nullptr) {
        ui->webviewLayout->removeWidget(webview);
        webview->deleteLater();
        webview = nullptr;
    }
    ui->webviewTarget->setVisible(conf.showWebView());

    if (conf.showWebView()) {
       webview = new QWebEngineView();
       ui->webviewLayout->addWidget(webview);
       webview->load(conf.webViewURL());
       webview->setFixedSize(conf.webViewWidth(), conf.webViewHeight());

       connect(webview, &QWebEngineView::loadFinished, this, &MainWindow::applyWebViewStyles);


    }

    adjustSize();
}

void MainWindow::createTriggers(Configuration &conf)
{
    while(ui->triggersLayout->count() > 0){
        ui->triggersLayout->itemAt(0)->widget()->deleteLater();
        ui->triggersLayout->removeItem(ui->triggersLayout->itemAt(0));
    }

    if (!conf.getShowTriggers())
        return;

    for(TriggerConfiguration t : conf.triggersList()) {
        TriggerWidget *widget = new TriggerWidget(t);
        connect(mMainTimer, &QTimer::timeout, widget, &TriggerWidget::refresh);
        ui->triggersLayout->addWidget(widget);
    }
}
