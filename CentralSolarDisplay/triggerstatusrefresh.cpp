#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>

#include "triggerstatusrefresh.h"

TriggerStatusRefresh::TriggerStatusRefresh(QObject *parent) : QObject(parent)
{

}

void TriggerStatusRefresh::requestRefresh(const TriggerConfiguration &conf)
{
    // Perform a request on the server
    QNetworkReply * reply = mAccessManager.get(
                QNetworkRequest(QUrl(conf.uRL())));

    connect(reply, &QNetworkReply::finished, this, &TriggerStatusRefresh::finished);

}

void TriggerStatusRefresh::finished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    reply->deleteLater();

    if(reply->error() != QNetworkReply::NoError){
        emit newStatusAvailable(TriggerStatus(TriggerStatusEnum::ERROR, 0));
        return;
    }

    QByteArray response = reply->readAll();

    QJsonObject obj = QJsonDocument::fromJson(response).object();

    bool isOn = obj.value("is_on").toBool();
    int runtime = obj.value("for").toInt();

    int totalUptime = obj.value("total_uptime").toInt(-1) + (isOn ? runtime : 0);
    int requiredUptime = obj.value("required_uptime").toInt(0);
    int progress = requiredUptime == 0 ? -1 : (totalUptime*100)/requiredUptime;

    emit newStatusAvailable(TriggerStatus(
                (isOn ? TriggerStatusEnum::ON
                                            : TriggerStatusEnum::OFF),
                runtime,
                progress
            ));
}
