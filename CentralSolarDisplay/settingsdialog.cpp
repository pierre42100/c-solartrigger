#include <QMessageBox>

#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "configurationhelper.h"

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    applyConfig();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::applyConfig()
{
    Configuration conf = ConfigurationHelper().getConfig();

    ui->showInverterProductionCheckBox->setChecked(conf.getShowInverterProduction());
    ui->productionURL->setText(conf.inverterProductionURL());

    ui->showTriggersCheckbox->setChecked(conf.getShowTriggers());
    ui->triggers->setPlainText(conf.triggers());

    ui->showStatusImageCheckbox->setChecked(conf.showStatusImage());
    ui->statusImageURL->setText(conf.statusImageURL());
    ui->statusImageHeight->setRange(1, 10000);
    ui->statusImageHeight->setValue(conf.statusImageHeight());
    ui->statusImageRefreshInterval->setRange(0, 100000);
    ui->statusImageRefreshInterval->setValue(conf.statusImageRefreshInterval());

    ui->refreshFreq->setValue(conf.refreshFrequency());
    ui->framelessWindowCheckbox->setChecked(conf.framelessWindow());
    ui->windowStayOnTop->setChecked(conf.windowStayOnTop());
    ui->centerWindowCheckBox->setChecked(conf.centerWindow());
    ui->windowPosX->setRange(0, 10000);
    ui->windowPosX->setValue(conf.windowPosX());
    ui->windowPosY->setRange(0, 10000);
    ui->windowPosY->setValue(conf.windowPosY());

    ui->showWebViewCheckBox->setChecked(conf.showWebView());
    ui->webViewWidth->setRange(0, 10000);
    ui->webViewWidth->setValue(conf.webViewWidth());
    ui->webViewHeight->setRange(0, 10000);
    ui->webViewHeight->setValue(conf.webViewHeight());
    ui->webViewURL->setText(conf.webViewURL());
    ui->webviewStyle->setPlainText(conf.webViewStyle());

    refresh();}

int SettingsDialog::exec()
{
    int result = QDialog::exec();

    // Save new configuration
    if(result == QDialog::Accepted){
        Configuration conf;

        conf.setShowInverterProduction(ui->showInverterProductionCheckBox->isChecked());
        conf.setInverterProductionURL(ui->productionURL->text());

        conf.setShowStatusImage(ui->showStatusImageCheckbox->isChecked());
        conf.setStatusImageURL(ui->statusImageURL->text());
        conf.setStatusImageHeight(ui->statusImageHeight->value());
        conf.setStatusImageRefreshInterval(ui->statusImageRefreshInterval->value());

        conf.setShowWebView(ui->showWebViewCheckBox->isChecked());
        conf.setWebViewWidth(ui->webViewWidth->value());
        conf.setWebViewHeight(ui->webViewHeight->value());
        conf.setWebViewStyle(ui->webviewStyle->toPlainText());
        conf.setWebViewURL(ui->webViewURL->text());

        conf.setShowTriggers(ui->showTriggersCheckbox->isChecked());
        conf.setTriggers(ui->triggers->toPlainText());

        conf.setRefreshFrequency(ui->refreshFreq->value());
        conf.setFramelessWindow(ui->framelessWindowCheckbox->isChecked());
        conf.setWindowStayOnTop(ui->windowStayOnTop->isChecked());
        conf.setCenterWindow(ui->centerWindowCheckBox->isChecked());
        conf.setWindowPosX(ui->windowPosX->value());
        conf.setWindowPosY(ui->windowPosY->value());

        ConfigurationHelper().setConfig(conf);
    }

    return result;
}

void SettingsDialog::refresh()
{
    ui->productionURL->setEnabled(ui->showInverterProductionCheckBox->isChecked());

    ui->triggers->setEnabled(ui->showTriggersCheckbox->isChecked());

    bool showStatusImage = ui->showStatusImageCheckbox->isChecked();
    ui->statusImageURL->setEnabled(showStatusImage);
    ui->statusImageHeight->setEnabled(showStatusImage);
    ui->statusImageRefreshInterval->setEnabled(showStatusImage);

    bool centerImage = ui->centerWindowCheckBox->isChecked();
    ui->windowPosX->setEnabled(!centerImage);
    ui->windowPosY->setEnabled(!centerImage);

    bool showWebview = ui->showWebViewCheckBox->isChecked();
    ui->webViewURL->setEnabled(showWebview);
    ui->webViewWidth->setEnabled(showWebview);
    ui->webViewHeight->setEnabled(showWebview);
    ui->webviewStyle->setEnabled(showWebview);
}

void SettingsDialog::on_showStatusImageCheckbox_toggled(bool)
{
    refresh();
}

void SettingsDialog::on_showInverterProductionCheckBox_toggled(bool)
{
    refresh();
}

void SettingsDialog::on_showTriggersCheckbox_toggled(bool)
{
    refresh();
}

void SettingsDialog::on_centerWindowCheckBox_toggled(bool)
{
    refresh();
}

void SettingsDialog::on_showWebViewCheckBox_toggled(bool)
{
    refresh();
}
