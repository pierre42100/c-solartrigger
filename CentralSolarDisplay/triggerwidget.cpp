#include <QMessageBox>

#include "triggerwidget.h"
#include "ui_triggerwidget.h"

TriggerWidget::TriggerWidget(const TriggerConfiguration &conf, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TriggerWidget), mConfig(conf)
{
    ui->setupUi(this);

    mRefresh = new TriggerStatusRefresh;
    connect(mRefresh, &TriggerStatusRefresh::newStatusAvailable,
            this, &TriggerWidget::newDataAvailable);
}

TriggerWidget::~TriggerWidget()
{
    delete ui;
}

void TriggerWidget::refresh()
{
    mRefresh->requestRefresh(mConfig);
}

void TriggerWidget::newDataAvailable(const TriggerStatus &status)
{
    this->mStatus = status;

    switch (status.status()) {

        case TriggerStatusEnum::ERROR:
            ui->statusIcon->setPixmap(QPixmap(QString::fromUtf8(":/img/ic_signal_wifi_off_black_48dp.png")));
            break;

        case TriggerStatusEnum::OFF:
            ui->statusIcon->setPixmap(QPixmap(QString::fromUtf8(":/img/ic_flash_off_black_48dp.png")));
            break;

        case TriggerStatusEnum::ON:
            ui->statusIcon->setPixmap(QPixmap(QString::fromUtf8(":/img/ic_flash_on_black_48dp.png")));
            break;

    }

}

void TriggerWidget::mouseDoubleClickEvent(QMouseEvent *)
{

    QString message = "Relay name: " + this->mConfig.name() + "\n" +
                      "Status: " + (this->mStatus.status() == TriggerStatusEnum::ON ? "On" : this->mStatus.status() == TriggerStatusEnum::OFF ? "Off" : "Offline") + "\n" +
                      "Since: " + this->mStatus.sinceStr() + "\n";

    if(this->mStatus.hasProgress())
        message += "Progress: " + QString::number(this->mStatus.progress()) + "%\n";


    QMessageBox::information(this,
                             tr("Relay status"),
                             message);
}
