/**
 * Project configuration
 *
 * @author Pierre HUBERT
 */

/**
 * Application information
 */
#define ORGANIZATION_NAME "Communiquons"
#define ORGANIZATION_DOMAIN "communiquons.org"
#define APPLICATION_NAME "CentralSolarDisplay"
