#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

    /**
     * Apply current configuration to the dialog
     */
    void applyConfig();

    int exec() override;

private slots:
    void on_showStatusImageCheckbox_toggled(bool);

    void on_showInverterProductionCheckBox_toggled(bool);

    void on_showTriggersCheckbox_toggled(bool);

    void on_centerWindowCheckBox_toggled(bool);

    void on_showWebViewCheckBox_toggled(bool);

private:

    void refresh();

    Ui::SettingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
