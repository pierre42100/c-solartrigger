#include "configuration.h"

Configuration::Configuration()
{

}

QString Configuration::inverterProductionURL() const
{
    return mInverterProductionURL;
}

void Configuration::setInverterProductionURL(const QString &inverterProductionURL)
{
    mInverterProductionURL = inverterProductionURL;
}

QString Configuration::triggers() const
{
    return mTriggers;
}

void Configuration::setTriggers(const QString &triggers)
{
    mTriggers = triggers;
}

QList<TriggerConfiguration> Configuration::triggersList() const
{
    QList<TriggerConfiguration> list;

    for(QString entry : triggers().split("\n")){

        if(!entry.contains(","))
            continue;

        QStringList info = entry.split(",");

        list.push_back(TriggerConfiguration(info[0], info[1]));
    }

    return list;
}

int Configuration::refreshFrequency() const
{
    return mRefreshFrequency;
}

void Configuration::setRefreshFrequency(int refreshFrequency)
{
    mRefreshFrequency = refreshFrequency;
}

bool Configuration::framelessWindow() const
{
    return mFramelessWindow;
}

void Configuration::setFramelessWindow(bool framelessWindow)
{
    mFramelessWindow = framelessWindow;
}

bool Configuration::windowStayOnTop() const
{
    return mWindowStayOnTop;
}

void Configuration::setWindowStayOnTop(bool windowStayOnTop)
{
    mWindowStayOnTop = windowStayOnTop;
}

bool Configuration::getShowInverterProduction() const
{
    return mShowInverterProduction;
}

void Configuration::setShowInverterProduction(bool value)
{
    mShowInverterProduction = value;
}

bool Configuration::showStatusImage() const
{
    return mShowStatusImage;
}

void Configuration::setShowStatusImage(bool value)
{
    mShowStatusImage = value;
}

int Configuration::statusImageRefreshInterval() const
{
    return mStatusImageRefreshInterval;
}

void Configuration::setStatusImageRefreshInterval(int statusImageRefreshInterval)
{
    mStatusImageRefreshInterval = statusImageRefreshInterval;
}

QString Configuration::statusImageURL() const
{
    return mStatusImageURL;
}

void Configuration::setStatusImageURL(const QString &statusImageURL)
{
    mStatusImageURL = statusImageURL;
}

bool Configuration::getShowTriggers() const
{
    return mShowTriggers;
}

void Configuration::setShowTriggers(bool showTriggers)
{
    mShowTriggers = showTriggers;
}

int Configuration::statusImageHeight() const
{
    return mStatusImageHeight;
}

void Configuration::setStatusImageHeight(int height)
{
    mStatusImageHeight = height;
}

bool Configuration::centerWindow() const
{
    return mCenterWindow;
}

void Configuration::setCenterWindow(bool centerWindow)
{
    mCenterWindow = centerWindow;
}

int Configuration::windowPosX() const
{
    return mWindowPosX;
}

void Configuration::setWindowPosX(int pos)
{
    mWindowPosX = pos;
}

int Configuration::windowPosY() const
{
    return mWindowPosY;
}

void Configuration::setWindowPosY(int pos)
{
    mWindowPosY = pos;
}

bool Configuration::showWebView() const
{
    return mShowWebView;
}

void Configuration::setShowWebView(bool showWebView)
{
    mShowWebView = showWebView;
}

int Configuration::webViewWidth() const
{
    return mWebViewWidth;
}

void Configuration::setWebViewWidth(int webViewWidth)
{
    mWebViewWidth = webViewWidth;
}

int Configuration::webViewHeight() const
{
    return mWebViewHeight;
}

void Configuration::setWebViewHeight(int webViewHeight)
{
    mWebViewHeight = webViewHeight;
}

QString Configuration::webViewURL() const
{
    return mWebViewURL;
}

void Configuration::setWebViewURL(const QString &webViewURL)
{
    mWebViewURL = webViewURL;
}

QString Configuration::webViewStyle() const
{
    return mWebViewStyle;
}

void Configuration::setWebViewStyle(const QString &webViewStyle)
{
    mWebViewStyle = webViewStyle;
}
