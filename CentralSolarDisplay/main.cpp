#include "mainwindow.h"
#include <QApplication>
#include <QGuiApplication>
#include <QScreen>

#include "config.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


    QCoreApplication::setOrganizationName(ORGANIZATION_NAME);
    QCoreApplication::setOrganizationDomain(ORGANIZATION_DOMAIN);
    QCoreApplication::setApplicationName(APPLICATION_NAME);


    MainWindow w;

    // Center the window on the screen
    QRect screenGeometry = QGuiApplication::screens().first()->geometry();
    int x = (screenGeometry.width()-w.width()) / 2;
    int y = (screenGeometry.height()-w.height()) / 2;
    w.move(x, y);

    w.show();

    return a.exec();
}
