/**
 * Trigger configuration
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QString>

class TriggerConfiguration
{
public:
    TriggerConfiguration(const QString &name, const QString &url);

    QString name() const;

    QString uRL() const;

private:
    QString mName;
    QString mURL;
};
