#include "inverterproductionrefresh.h"
#include "configurationhelper.h"

#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>

InverterProductionRefresh::InverterProductionRefresh(QObject *parent) : QObject(parent)
{

}

void InverterProductionRefresh::requestRefresh()
{

    // Perform a request on the server
    QNetworkReply * reply = mAccessManager.get(
                QNetworkRequest(QUrl(
                                    ConfigurationHelper().getConfig().inverterProductionURL()
                                    )
                                )
                );

    connect(reply, &QNetworkReply::finished, this, &InverterProductionRefresh::finished);

}

void InverterProductionRefresh::finished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    reply->deleteLater();

    if(reply->error() != QNetworkReply::NoError){
        emit gotNewValues(InverterProduction(-1, -1));
        return;
    }

    QByteArray response = reply->readAll();

    QJsonObject obj = QJsonDocument::fromJson(response).object()
            .value("Body").toObject()
            .value("Data").toObject()
            .value("Site").toObject();

    emit gotNewValues(InverterProduction(
                          obj.value("P_PV").toInt(),
                          obj.value("P_Grid").toDouble()));
}
