#include "triggerconfiguration.h"

TriggerConfiguration::TriggerConfiguration(
        const QString &name, const QString &url) :
    mName(name), mURL(url)
{

}

QString TriggerConfiguration::name() const
{
    return mName;
}

QString TriggerConfiguration::uRL() const
{
    return mURL;
}
