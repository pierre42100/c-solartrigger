/**
 * Main application window
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QMainWindow>
#include <QTimer>

#include <QWebEngineView>

#include "configuration.h"
#include "inverterproduction.h"
#include "inverterproductionrefresh.h"
#include "statusimagerefresh.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void refreshTimeout();

    void refreshStatusImageTimeout();

    void newInverterProduction(const InverterProduction &prod);

    void newStatusImage(const QPixmap pm);

    void applyWebViewStyles();

    void on_actionSettings_triggered();

    void on_actionQuit_triggered();

private:

    void refreshUI();

    void createTriggers(Configuration &conf);

    Ui::MainWindow *ui;
    QTimer *mMainTimer = nullptr;
    QTimer *mStatusImageMainTimer = nullptr;
    InverterProductionRefresh *mInverterProdRefresh;
    StatusImageRefresh *mStatusImageRefresh;
    QWebEngineView *webview = nullptr;
};
