/**
 * Counter widget
 *
 * A widget that refresh every seconds to display a chronometer
 * value
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QLabel>

class CounterWidget : public QLabel
{
    Q_OBJECT
public:
    explicit CounterWidget(QWidget *parent = 0);

    int currentTime() const;
    void setCurrentTime(int currentTime);

signals:

public slots:
    void refresh();

private slots:
    void tick();

private:
    int mCurrentTime = 0;
};
