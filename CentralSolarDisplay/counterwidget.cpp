#include <QTimer>

#include "counterwidget.h"

CounterWidget::CounterWidget(QWidget *parent) : QLabel(parent)
{
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &CounterWidget::tick);
    timer->start(1000);
}

int CounterWidget::currentTime() const
{
    return mCurrentTime;
}

void CounterWidget::setCurrentTime(int currentTime)
{
    mCurrentTime = currentTime;
    refresh();
}

void CounterWidget::refresh()
{
    int s = mCurrentTime % 60;
    int m = (mCurrentTime / 60) % 60;
    int h = mCurrentTime / 60 / 60;

    setText(QString::number(h) + " h " +
            QString::number(m) + " m " +
            QString::number(s) + " s");
}

void CounterWidget::tick()
{
    mCurrentTime++;
    refresh();
}
