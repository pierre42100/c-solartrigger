/**
 * Inverter production refresh "worker"
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QObject>
#include <QNetworkAccessManager>

#include "inverterproduction.h"

class InverterProductionRefresh : public QObject
{
    Q_OBJECT
public:
    explicit InverterProductionRefresh(QObject *parent = 0);

    void requestRefresh();

signals:

    void gotNewValues(const InverterProduction &newProd);

public slots:

    /**
     * Requests callback slots
     */
    void finished();


private slots:


private:
    QNetworkAccessManager mAccessManager;
};
