/**
 * Inverter production. Contains the state of production
 * of the inverter at a precise given time
 *
 * @author Pierre HUBERT
 */

#pragma once


class InverterProduction
{
public:
    InverterProduction(int solar, int edf);

    int solarProduction() const;

    int eDFProduction() const;

private:
    int mSolarProduction;
    int mEDFProduction;
};
