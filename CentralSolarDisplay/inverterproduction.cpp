#include "inverterproduction.h"

InverterProduction::InverterProduction(int solar, int edf)
    : mSolarProduction(solar), mEDFProduction(edf)
{

}

int InverterProduction::solarProduction() const
{
    return mSolarProduction;
}

int InverterProduction::eDFProduction() const
{
    return mEDFProduction;
}
