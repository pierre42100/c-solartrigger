/**
 * Trigger status fetcher
 *
 * @author Pierre HUBERT
 */

#pragma once

#include "triggerconfiguration.h"
#include "triggerstatus.h"

#include <QNetworkAccessManager>
#include <QObject>

class TriggerStatusRefresh : public QObject
{
    Q_OBJECT
public:
    explicit TriggerStatusRefresh(QObject *parent = 0);

public slots:
    void requestRefresh(const TriggerConfiguration &conf);

signals:

    void newStatusAvailable(const TriggerStatus &status);

public slots:

private slots:

    /**
     * Called when network request finishes
     */
    void finished();

private:
    QNetworkAccessManager mAccessManager;
};
