#include <QSettings>

#include "configurationhelper.h"

#define SHOW_INVERTER_PRODUCTION_KEY "show_inverter_production"
#define INVERTER_PROD_URL_KEY "inverter_prod_url"

#define SHOW_TRIGGERS_KEY "show_triggers_key"
#define TRIGGERS_CONF_KEY "triggers_key"

#define REFRESH_FREQ_KEY "refresh_freq"
#define FRAMELESS_WINDOW_KEY "frameless_window"
#define WINDOW_ON_TOP_KEY "window_on_top"
#define CENTER_WINDOW_KEY "center_window"
#define WINDOW_POS_X_KEY "window_pos_x"
#define WINDOW_POS_Y_KEY "window_pos_y"

#define SHOW_STATUS_IMAGE_KEY "enable_status_image"
#define STATUS_IMAGE_URL_KEY "status_image_url"
#define STATUS_IMAGE_HEIGHT_KEY "status_image_height"
#define STATUS_IMAGE_REFRESH_INTERVAL_KEY "status_image_refresh_interval"

#define SHOW_WEBVIEW_KEY "show_webview"
#define WEBVIEW_WIDTH_KEY "webview_width"
#define WEBVIEW_HEIGHT_KEY "webview_height"
#define WEBVIEW_STYLE_KEY "webview_style"
#define WEBVIEW_URL_KEY "webview_url"

ConfigurationHelper::ConfigurationHelper(QObject *parent) : QObject(parent)
{

}

Configuration ConfigurationHelper::getConfig()
{
    Configuration conf;
    QSettings settings;

    conf.setShowInverterProduction(settings.value(SHOW_INVERTER_PRODUCTION_KEY, QVariant(true)).toBool());
    conf.setInverterProductionURL(
                settings.value(INVERTER_PROD_URL_KEY, "").toString());

    conf.setShowTriggers(settings.value(SHOW_TRIGGERS_KEY, QVariant(true)).toBool());
    conf.setTriggers(settings.value(TRIGGERS_CONF_KEY, "").toString());

    conf.setShowStatusImage(settings.value(SHOW_STATUS_IMAGE_KEY, QVariant(false)).toBool());
    conf.setStatusImageURL(settings.value(STATUS_IMAGE_URL_KEY, "").toString());
    conf.setStatusImageHeight(settings.value(STATUS_IMAGE_HEIGHT_KEY, QVariant(100)).toInt());
    conf.setStatusImageRefreshInterval(settings.value(STATUS_IMAGE_REFRESH_INTERVAL_KEY, QVariant(500)).toInt());

    conf.setRefreshFrequency(settings.value(REFRESH_FREQ_KEY, QVariant(6)).toInt());
    conf.setFramelessWindow(settings.value(FRAMELESS_WINDOW_KEY, QVariant(false)).toBool());
    conf.setWindowStayOnTop(settings.value(WINDOW_ON_TOP_KEY, QVariant(true)).toBool());
    conf.setCenterWindow(settings.value(CENTER_WINDOW_KEY, QVariant(true)).toBool());
    conf.setWindowPosX(settings.value(WINDOW_POS_X_KEY, QVariant(10)).toInt());
    conf.setWindowPosY(settings.value(WINDOW_POS_Y_KEY, QVariant(10)).toInt());

    conf.setShowWebView(settings.value(SHOW_WEBVIEW_KEY, QVariant(false)).toBool());
    conf.setWebViewWidth(settings.value(WEBVIEW_WIDTH_KEY, QVariant(100)).toInt());
    conf.setWebViewHeight(settings.value(WEBVIEW_HEIGHT_KEY, QVariant(100)).toInt());
    conf.setWebViewStyle(settings.value(WEBVIEW_STYLE_KEY, "").toString());
    conf.setWebViewURL(settings.value(WEBVIEW_URL_KEY, "").toString());

    return conf;
}

void ConfigurationHelper::setConfig(const Configuration &conf)
{
    QSettings settings;

    settings.setValue(SHOW_INVERTER_PRODUCTION_KEY, conf.getShowInverterProduction());
    settings.setValue(INVERTER_PROD_URL_KEY, conf.inverterProductionURL());

    settings.setValue(SHOW_TRIGGERS_KEY, conf.getShowTriggers());
    settings.setValue(TRIGGERS_CONF_KEY, conf.triggers());

    settings.setValue(SHOW_STATUS_IMAGE_KEY, conf.showStatusImage());
    settings.setValue(STATUS_IMAGE_URL_KEY, conf.statusImageURL());
    settings.setValue(STATUS_IMAGE_HEIGHT_KEY, conf.statusImageHeight());
    settings.setValue(STATUS_IMAGE_REFRESH_INTERVAL_KEY, conf.statusImageRefreshInterval());

    settings.setValue(REFRESH_FREQ_KEY, conf.refreshFrequency());
    settings.setValue(FRAMELESS_WINDOW_KEY, conf.framelessWindow());
    settings.setValue(WINDOW_ON_TOP_KEY, conf.windowStayOnTop());
    settings.setValue(CENTER_WINDOW_KEY, conf.centerWindow());
    settings.setValue(WINDOW_POS_X_KEY, conf.windowPosX());
    settings.setValue(WINDOW_POS_Y_KEY, conf.windowPosY());

    settings.setValue(SHOW_WEBVIEW_KEY, conf.showWebView());
    settings.setValue(WEBVIEW_WIDTH_KEY, conf.webViewWidth());
    settings.setValue(WEBVIEW_HEIGHT_KEY, conf.webViewHeight());
    settings.setValue(WEBVIEW_STYLE_KEY, conf.webViewStyle());
    settings.setValue(WEBVIEW_URL_KEY, conf.webViewURL());
}
