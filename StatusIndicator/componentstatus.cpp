#include "componentstatus.h"

ComponentStatus::ComponentStatus()
{

}

bool ComponentStatus::getIs_on() const
{
    return is_on;
}

void ComponentStatus::setIs_on(bool value)
{
    is_on = value;
}

unsigned int ComponentStatus::getFor_secs() const
{
    return for_secs;
}

void ComponentStatus::setFor_secs(unsigned int value)
{
    for_secs = value;
}

int ComponentStatus::getCurr_prod() const
{
    return curr_prod;
}

void ComponentStatus::setCurr_prod(int value)
{
    curr_prod = value;
}

bool ComponentStatus::getIs_valid() const
{
    return is_valid;
}

void ComponentStatus::setIs_valid(bool value)
{
    is_valid = value;
}
