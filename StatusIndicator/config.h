/**
 * Project configuration
 *
 * @author Pierre HUBERT
 */

#pragma once

/**
 * The URL of the component that provides production information
 */
#define COMPONENT_URL "http://localhost:8089/api/status"

/**
 * Refresh interval (in ms)
 */
#define REFRESH_INTERVAL 3000
