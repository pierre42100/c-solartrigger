#include "mainwidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Configure application information
    QCoreApplication::setApplicationName("SolarTriggerStatusIndicator");
    QCoreApplication::setOrganizationName("Communiquons");
    QCoreApplication::setOrganizationDomain("communiquons.org");

    MainWidget w;
    w.show();

    return a.exec();
}
