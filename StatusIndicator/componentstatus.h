#ifndef COMPONENTSTATUS_H
#define COMPONENTSTATUS_H


class ComponentStatus
{
public:
    ComponentStatus();

    bool getIs_on() const;
    void setIs_on(bool value);

    unsigned int getFor_secs() const;
    void setFor_secs(unsigned int value);

    int getCurr_prod() const;
    void setCurr_prod(int value);

    bool getIs_valid() const;
    void setIs_valid(bool value);

private:

    //Private fields
    bool is_valid = false;
    bool is_on;
    unsigned int for_secs;
    int curr_prod;
};

#endif // COMPONENTSTATUS_H
