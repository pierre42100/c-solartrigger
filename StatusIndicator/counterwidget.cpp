#include <QTimer>

#include "counterwidget.h"

CounterWidget::CounterWidget(QWidget *parent)
    : QLabel(parent)
{
    refresh();

    mTimer = new QTimer();
    connect(mTimer, &QTimer::timeout, this, &CounterWidget::oneMoreSec);
    mTimer->start(1000);
}

CounterWidget::~CounterWidget()
{
    mTimer->deleteLater();
}

int CounterWidget::getCount() const
{
    return count;
}

void CounterWidget::setCount(int value)
{
    count = value;
    refresh();
}

void CounterWidget::refresh()
{
    setText(QString::number(count));
}

void CounterWidget::oneMoreSec()
{
    setCount(getCount() + 1);
}
