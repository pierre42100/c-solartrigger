#include "settingswidget.h"
#include "ui_settingswidget.h"
#include "settings.h"
#include "locationpicker.h"

SettingsWidget::SettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsWidget)
{
    ui->setupUi(this);
    Settings settings;
    ui->url->setText(settings.getUrl());
    ui->window_pos_x->setValue(settings.getPosition().x());
    ui->window_pos_y->setValue(settings.getPosition().y());
}

SettingsWidget::~SettingsWidget()
{
    delete ui;
}

void SettingsWidget::on_buttonBox_accepted()
{
    Settings settings;
    settings.setUrl(ui->url->text());
    settings.setPosition(QPoint(ui->window_pos_x->value(),
                                ui->window_pos_y->value()));
    close();
}

void SettingsWidget::on_buttonBox_rejected()
{
    close();
}

void SettingsWidget::on_pushButton_clicked()
{
    QPoint point = LocationPicker().pick();
    ui->window_pos_x->setValue(point.x());
    ui->window_pos_y->setValue(point.y());
}
