/**
 * Counter widget - a small widget that refreshes
 * every seconds and add one each second
 *
 * @author Pierre HUBERT
 */

#ifndef COUNTERWIDGET_H
#define COUNTERWIDGET_H

#include <QLabel>

class CounterWidget : public QLabel
{
    Q_OBJECT
public:
    explicit CounterWidget(QWidget *parent = 0);
    ~CounterWidget();

    int getCount() const;
    void setCount(int value);

signals:

public slots:
    void refresh();

private slots:
    void oneMoreSec();

private:
    int count = 0;
    QTimer *mTimer;
};

#endif // COUNTERWIDGET_H
