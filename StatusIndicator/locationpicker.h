#ifndef LOCATIONPICKER_H
#define LOCATIONPICKER_H

#include <QDialog>

namespace Ui {
class LocationPicker;
}

class LocationPicker : public QDialog
{
    Q_OBJECT

public:
    explicit LocationPicker(QWidget *parent = 0);
    ~LocationPicker();

    //Pick a location with this widget
    QPoint pick();

protected:
    void mousePressEvent(QMouseEvent *event);

private:
    Ui::LocationPicker *ui;
    QPoint mPos;
};

#endif // LOCATIONPICKER_H
