/**
 * Application settings
 *
 * @author Pierre HUBERT
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
class QSettings;

class Settings
{
public:
    Settings();

    //URL settings
    bool hasUrl();
    QString getUrl();
    void setUrl(const QString &value);


    //Window position
    bool hasPosition();
    QPoint getPosition();
    void setPosition(const QPoint &point);
};

#endif // SETTINGS_H
