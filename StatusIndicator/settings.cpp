#include <QCoreApplication>
#include <QSettings>
#include <QPoint>

#include "settings.h"

static QSettings *mSettings = nullptr;

Settings::Settings()
{
    // Initialize settings if required
    if(mSettings == nullptr) {
        mSettings = new QSettings;
        QObject::connect(QCoreApplication::instance(),
                         &QCoreApplication::aboutToQuit,
                         mSettings,
                         &QSettings::deleteLater);
    }
}

bool Settings::hasUrl()
{
    return mSettings->contains("url");
}

QString Settings::getUrl()
{
    return mSettings->value("url", "").toString();
}

void Settings::setUrl(const QString &value)
{
    return mSettings->setValue("url", value);
}

bool Settings::hasPosition()
{
    return mSettings->contains("pos_x")
            && mSettings->contains("pos_y");
}

QPoint Settings::getPosition()
{
    return QPoint(mSettings->value("pos_x", 0).toInt(),
                  mSettings->value("pos_y", 0).toInt());
}

void Settings::setPosition(const QPoint &point)
{
    mSettings->setValue("pos_x", point.x());
    mSettings->setValue("pos_y", point.y());
}
