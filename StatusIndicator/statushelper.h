/**
 * Status helper
 *
 * This files allows to retrieve and return the status
 * of a component
 *
 * @author Pierre HUBERT
 */

#ifndef STATUSHELPER_H
#define STATUSHELPER_H

#include <QObject>
#include <QNetworkAccessManager>

#include "componentstatus.h"

class StatusHelper : public QObject
{
    Q_OBJECT
public:
    explicit StatusHelper(QObject *parent = 0);

signals:
    /**
     * This signal is emitted when a new status become available
     *
     * @param s New status
     */
    void statusAvailable(const ComponentStatus &s);

public slots:

    /**
     * Get new component status
     */
    void getStatus();

private slots:

    //Network request callbacks
    void requestError();
    void requestFinished();

private:

    //Private fields
    QNetworkAccessManager mNetworkAccessManager;
};

#endif // STATUSHELPER_H
