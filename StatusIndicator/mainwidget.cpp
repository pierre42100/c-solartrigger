#include <QTimer>

#include "mainwidget.h"
#include "ui_mainwidget.h"
#include "config.h"
#include "settings.h"
#include "settingswidget.h"

MainWidget::MainWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    mCounterWidget = new CounterWidget();
    ui->mainLayout->addWidget(mCounterWidget);

    //Initialize status helper
    mStatusHelper = new StatusHelper(this);
    connect(mStatusHelper, &StatusHelper::statusAvailable, this, &MainWidget::gotStatus);

    //Update at regular interval component status
    mTimer = new QTimer();
    mTimer->start(REFRESH_INTERVAL);
    connect(mTimer, &QTimer::timeout, mStatusHelper, &StatusHelper::getStatus);

    //Check if settings have been defined or not
    if(!Settings().hasUrl())
        (new SettingsWidget())->show();

    // Move the window to the specified position
    if(Settings().hasPosition())
        move(Settings().getPosition());
}

MainWidget::~MainWidget()
{
    delete ui;
    mStatusHelper->deleteLater();
    mTimer->deleteLater();
}

void MainWidget::refresh()
{
    if(!mCurrentStatus.getIs_valid()){
        ui->statusLabel->setText(tr("Error!"));
        return;
    }

    ui->onImage->setVisible(mCurrentStatus.getIs_on());
    ui->statusLabel->setText(mCurrentStatus.getIs_on() ? tr("ON") : tr("OFF"));
    ui->prod->display(mCurrentStatus.getCurr_prod());
    mCounterWidget->setCount(mCurrentStatus.getFor_secs());
}

void MainWidget::gotStatus(const ComponentStatus &status)
{
    mCurrentStatus = status;
    refresh();
}

void MainWidget::on_actionOpen_settings_triggered()
{
    (new SettingsWidget())->show();
}

void MainWidget::on_actionQuit_triggered()
{
    QCoreApplication::exit();
}
