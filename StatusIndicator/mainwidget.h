#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QMainWindow>

#include "componentstatus.h"
#include "statushelper.h"
#include "counterwidget.h"

namespace Ui {
class MainWidget;
}

class MainWidget : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();
    void refresh();

private slots:

    /**
     * Slot called when new status information becomes available
     *
     * @param status New status information
     */
    void gotStatus(const ComponentStatus &status);

    /** Open settings */
    void on_actionOpen_settings_triggered();

    /** Quit application */
    void on_actionQuit_triggered();

private:

    //Private fields
    QTimer *mTimer;
    Ui::MainWidget *ui;
    StatusHelper *mStatusHelper;
    ComponentStatus mCurrentStatus;
    CounterWidget *mCounterWidget;
};

#endif // MAINWIDGET_H
