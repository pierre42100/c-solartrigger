#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>

#include "statushelper.h"
#include "config.h"
#include "settings.h"

StatusHelper::StatusHelper(QObject *parent) : QObject(parent)
{
    //Nothing yet
}

void StatusHelper::getStatus()
{
    //Execute request and make connections

    QNetworkRequest request(QUrl(Settings().getUrl()));
    QNetworkReply *reply = mNetworkAccessManager.get(request);

    connect(reply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error),
          [=](QNetworkReply::NetworkError){ requestError(); });
    connect(reply, &QNetworkReply::finished, this, &StatusHelper::requestFinished);
}

void StatusHelper::requestError()
{
    ComponentStatus c;
    c.setIs_valid(false);
    emit statusAvailable(c);
}

void StatusHelper::requestFinished()
{
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());

    //Check response code
    if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute) != 200){
        requestError();
        return;
    }

    //Get response content
    QJsonObject obj = QJsonDocument::fromJson(reply->readAll()).object();
    ComponentStatus c;
    c.setIs_valid(obj.contains("is_on") && obj.contains("for") && obj.contains("prod"));
    c.setIs_on(obj.value("is_on").toBool());
    c.setFor_secs(obj.value("for").toInt());
    c.setCurr_prod(obj.value("prod").toInt());
    emit statusAvailable(c);
}
