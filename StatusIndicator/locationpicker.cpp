#include <QGuiApplication>
#include <QScreen>
#include <QMouseEvent>

#include "locationpicker.h"
#include "ui_locationpicker.h"

LocationPicker::LocationPicker(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LocationPicker)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);
    move(0, 0);
    setFixedSize(QGuiApplication::primaryScreen()->size());
}

LocationPicker::~LocationPicker()
{
    delete ui;
}

QPoint LocationPicker::pick()
{
    exec();
    return mPos;
}

void LocationPicker::mousePressEvent(QMouseEvent *event)
{
    mPos = event->pos();
    close();
}
