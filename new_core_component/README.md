# NewCoreComponent
New version of solar trigger, improved.

## Installation

```sh
sudo apt install -y libcurlpp-dev libcurl4-gnutls-dev
```

## Installing as service
```sh
sudo cp solar_trigger.service /etc/systemd/system/
sudo systemctl start solar_trigger
sudo systemctl enable solar_trigger
```

## Test service
```sh
curl http://[RASPI_URL]:8089/api/status
```

## Test on a computer (disable real GPIO manipulation)
```sh
make all ON_RASPBERRY=0
```
