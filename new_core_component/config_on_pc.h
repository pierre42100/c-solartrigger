#define SERVER_ADDRESS "http://localhost:8088/"

// Refresh intervall in millisecs
#define REFRESH_INTERVAL 1000

// In case of failure to connect
#define DEFAULT_PROD 0

// Minimum of time the production must be valid before the relay is started (seconds)
#define CHECK_TIME 5

// Minimum of time the device will be up (seconds)
#define MINIMAL_RUNTIME 10

// Minimum of time the device will be down (seconds)
#define MINIMAL_DOWNTIME 10

// The thresold below which one the relay will be triggered
#define PRODUCTION_LIMIT -100

// Consumption of the target device
#define DEVICE_CONSUMPTION 50

// Local server info
#define INTERNAL_SERVER_NAME "SolarTriggerCore"
#define INTERNAL_SERVER_PORT 8089

// GPIO port number used to control the relay (BCM numerotation)
#define GPIO_PORT_NUMBER 21

// Parent relay status URL (if any) ==> This relay will have to be up in order to get this controller work
// Comment this to disable the feature
#define PARENT_RELAY_STATUS_URL "http://localhost:3000/new_core_component/status.json"