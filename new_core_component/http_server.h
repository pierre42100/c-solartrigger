/**
 * Simple HTTP Server
 *
 * @author Pierre HUBERT
 */

#pragma once

/**
 * Server structure
 */
typedef struct http_server_t http_server_t;


/**
 * Request format
 */
typedef struct http_server_request_t {
	char *uri;
	char *host;
	char *client_address;
	char *user_agent;
} http_server_request_t;

/**
 * Response format
 */
typedef struct http_server_response_t {
	int code;
	char *content; /* Will be freed on response */
	char *content_type;
	char *headers;
} http_server_response_t;

/**
 * Route callback
 */
typedef void (*route_callback) (const http_server_request_t*, 
	http_server_response_t*);

/**
 * Initialize server
 *
 * @returns Empty HTTP server
 */
http_server_t * http_server_init();


/**
 * Quit server
 *
 * @param server
 */
void http_server_quit(http_server_t * server);


/**
 * Add a route to the server
 *
 * @param server The target server
 * @param uri The URI of the route
 * @param cb Callback function to call
 */
void http_server_add_route(http_server_t * server, 
	const char *uri, route_callback cb);

/**
 * Serve
 *
 * @param server
 * @param port The port to listen on
 * @param serverName The name of the server
 */
void http_server_serve(http_server_t * server, 
	int port,
	const char * serverName);
