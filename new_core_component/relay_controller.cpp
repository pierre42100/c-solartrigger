/*
 * Inspired from from https://elinux.org/RPi_GPIO_Code_Samples#sysfs
 *
 * @author Pierre HUBERT
 */

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"
#include "relay_controller.h"

#define IN  0
#define OUT 1

#define LOW  0
#define HIGH 1

#define ENABLE 0
#define DISABLE 1


#define FATAL_ERROR(msg) {fprintf(stderr, "FATAL: %s\n", msg); exit(-1);}
#define LOG_ERROR(msg) {fprintf(stderr, "ERR: %s\n", msg);}

static int
GPIOExport(int pin)
{
#define BUFFER_MAX 3
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open export for writing!\n");
		return(-1);
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}

static int
GPIOUnexport(int pin)
{
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open unexport for writing!\n");
		return(-1);
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}

static int
GPIODirection(int pin, int dir)
{
	static const char s_directions_str[]  = "in\0out";

#define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;

	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio direction for writing!\n");
		return(-1);
	}

	if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3)) {
		fprintf(stderr, "Failed to set direction!\n");
		return(-1);
	}

	close(fd);
	return(0);
}

static int
GPIOWrite(int pin, int value)
{
	#define VALUE_MAX 30
	static const char s_values_str[] = "01";

	char path[VALUE_MAX];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		fprintf(stderr, "Failed to open gpio value for writing!\n");
		return(-1);
	}

	if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1)) {
		fprintf(stderr, "Failed to write value!\n");
		return(-1);
	}

	close(fd);
	return(0);
}





void relay_controller_init() {

	if(ON_RASPBERRY) {
		/*
		 * Enable GPIO pins
		 */
		if (-1 == GPIOExport(GPIO_PORT_NUMBER))
			FATAL_ERROR("Could not export pin (running as root ?)");

		/*
		 * Set GPIO directions
		 */
		if (-1 == GPIODirection(GPIO_PORT_NUMBER, OUT))
			FATAL_ERROR("Could not set pin direction !");
	}

	// Disable relay by default
	relay_controller_switch(false);
}


void relay_controller_quit() {

	// Disable relay
	relay_controller_switch(false);

	if (ON_RASPBERRY
		&& -1 == GPIOUnexport(GPIO_PORT_NUMBER))
		FATAL_ERROR("Could not stop GPIO!");
}

void relay_controller_switch(bool enable) {

	printf("NEW RELAY STATUS: %s\n", enable ? "YES" : "NO");
	if(ON_RASPBERRY 
		&& -1 == GPIOWrite(GPIO_PORT_NUMBER, enable ? ENABLE : DISABLE))
		LOG_ERROR("Could not switch relay status !");

}