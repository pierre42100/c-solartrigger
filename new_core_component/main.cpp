#include <iostream>
#include <sstream>

#include <time.h>
#include <unistd.h>
#include <string.h>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

#include "config.h"
#include "http_server.h"
#include "relay_controller.h"

using namespace std;
using namespace curlpp::options;

typedef struct status {
	int curr_prod;
	bool isOn;
	int since;
	int validSince;
} status;


static status current_status;

static int get_prod() {
	try
	{
		// RAII cleanup
		curlpp::Cleanup myCleanup;

#ifdef PARENT_RELAY_STATUS_URL

		// Check if parent relay is up
		{
			try {
				// Send request and get a result.
				std::ostringstream os;
				os << curlpp::options::Url(std::string(PARENT_RELAY_STATUS_URL));
				auto result = os.str();

				// Check if the relay is up, else refuse to go further
				if(result.find("\"is_on\": true") == string::npos) {
					cout << " Parent relay does not seems to be up. Using default production value" << endl;
					return DEFAULT_PROD;
				}
			}

			catch(exception &e) {
				std::cout << e.what() << std::endl;
				std::cout << "Uplink server seems to be offline. Skipping parent check..." << endl;
				// Do nothing, we consider the uplink server as unreachable
			}
		}
#endif

		// Send request and get a result.
		std::ostringstream os;
		os << curlpp::options::Url(std::string(SERVER_ADDRESS));
		auto result = os.str();

		// Parse response
		// Extract required value
		auto begin_pos = result.find(':', result.find("P_Grid")) + 1;
		while(result[begin_pos] == ' ') begin_pos++;
		auto end_pos = result.find(',', begin_pos);

		// Remove "." in value, if any
		auto first_dot_pos = result.find('.', begin_pos);
		if(first_dot_pos  <  end_pos)
			end_pos = result.find('.', begin_pos);

		return stoi(result.substr(begin_pos, end_pos));
	}

	catch(curlpp::RuntimeError & e)
	{
		std::cout << e.what() << std::endl;
		return DEFAULT_PROD;
	}

	catch(curlpp::LogicError & e)
	{
		std::cout << e.what() << std::endl;
		return DEFAULT_PROD;
	}

	catch(exception &e) {
		std::cout << e.what() << std::endl;
		return DEFAULT_PROD;
	}
}

static void setStatus(bool isOn) {
	cout << "Set status to " << (isOn ? "ON" : "OFF") << endl;

	// Update information
	current_status.isOn = isOn;
	current_status.since = time(NULL);
	current_status.validSince = -1;

	relay_controller_switch(isOn);
}

/**
 * Get current relay status route
 */
static void server_route_status(const http_server_request_t *req, 
	http_server_response_t *res) {

	//Basic information
	res->code = 200;
	res->content_type = strdup("application/json");


	char msg[200];
	sprintf(
		msg, 
		"{\"is_on\": %s,\"for\": %lu,\"prod\": %d}",
		current_status.isOn ? "true" : "false",
		current_status.since == 0 ? 0 : time(NULL) - current_status.since,
		current_status.curr_prod
	);

	res->content =  strdup(msg);
}

static void  * startServer(void* data) {

	// Initialize HTTP Server
	http_server_t *server = http_server_init();
	http_server_add_route(server, "/api/status", &server_route_status);
	printf("Listen on port %d\n", INTERNAL_SERVER_PORT);
	http_server_serve(server, INTERNAL_SERVER_PORT, INTERNAL_SERVER_NAME);

	http_server_quit(server);

	return NULL;
}

int main(int argc, char** argv) {

	cout << "New solar trigger" << endl;

	cout << "Init trigger" << endl;
	relay_controller_init();

	current_status.curr_prod = DEFAULT_PROD;
	setStatus(false);

	pthread_t serverThread;
	if(pthread_create(&serverThread, NULL, startServer, NULL) != 0) {
		fprintf(stderr, "Could not start server !\n");
		perror("Start server");
		return -1;
	}


	while(true) {
			
		// Refresh production information
		int new_prod = get_prod();
		cout << "Current production: " << new_prod << endl;

		// Check to turn on
		if(!current_status.isOn 
			&& current_status.since + MINIMAL_DOWNTIME < time(NULL)
			&& new_prod < PRODUCTION_LIMIT - DEVICE_CONSUMPTION) {

			if(current_status.validSince < 1)
				current_status.validSince = time(NULL);
			else if(current_status.validSince + CHECK_TIME < time(NULL))
				setStatus(true);
		}
		else
			current_status.validSince = 0;

		// Check to turn off
		if(current_status.isOn 
			&& current_status.since + MINIMAL_RUNTIME < time(NULL) 
			&& new_prod > PRODUCTION_LIMIT) {

			setStatus(false);
		}


		current_status.curr_prod = new_prod;

		usleep(REFRESH_INTERVAL*1000);
	}

	relay_controller_quit();

	return 0;
}