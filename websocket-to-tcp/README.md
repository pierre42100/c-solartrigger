# WebSocket to TCP
This project allows to spin up a TCP server that will forward all its traffic to an WebSocket.

This project is written in Rust.

This project aims specifically to turn Victron Envergny VNC Websocket to be turned into TCP socket.

## Service file
Located in: `/etc/systemd/system/websocket-to-tcp.service`

```
[Unit]
Description=Production Console Websocket to TCP service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=pi
ExecStart=/usr/bin/websocket-to-tcp 0.0.0.0 2564 ws://192.168.1.22:81/

[Install]
WantedBy=multi-user.target
```