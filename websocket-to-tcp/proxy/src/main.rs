use std::error::Error;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::sync::mpsc;
use std::time::Duration;

use websockets::{Frame, WebSocket, WebSocketReadHalf, WebSocketWriteHalf};

type Res = Result<(), Box<dyn Error>>;

enum MessageToSend {
    Close,
    Flush,
    Data(Vec<u8>),
}

fn handle_error(loc: &str, e: Res) {
    if let Err(err) = e {
        eprintln!("Error in {}: {}", loc, err);
    }
}

fn read_tcp_thread(mut s: TcpStream, channel: mpsc::Sender<MessageToSend>) -> Res {
    let mut buff = [0; 1024];

    loop {
        let read = s.read(&mut buff);

        if read.is_err() {
            channel.send(MessageToSend::Close)?;
        }

        let read = read?;

        channel.send(MessageToSend::Data(buff[0..read].to_vec()))?;
    }
}

fn send_flush_thread(channel: mpsc::Sender<MessageToSend>) -> Res {
    loop {
        channel.send(MessageToSend::Flush)?;
        std::thread::sleep(Duration::from_secs(2));
    }
}

async fn handle_write_half(s: TcpStream, mut write: WebSocketWriteHalf) -> Res {
    let (sender, receiver) = mpsc::channel::<MessageToSend>();
    let sender_2 = sender.clone();

    std::thread::spawn(move || handle_error("flush_thread", send_flush_thread(sender_2)));

    std::thread::spawn(move || handle_error("read_tcp_thread", read_tcp_thread(s, sender)));

    for msg in receiver.iter() {
        match msg {
            MessageToSend::Close => {
                return Ok(());
            }
            MessageToSend::Flush => {
                write.flush().await?;
            }
            MessageToSend::Data(d) => {
                write.send(Frame::Binary {
                    continuation: false,
                    fin: true,
                    payload: d,
                }).await?;
            }
        }
    }

    Ok(())
}

async fn handle_read_half(mut s: TcpStream, mut read: WebSocketReadHalf) -> Res {
    loop {
        let frame = read.receive().await?;

        match frame {
            Frame::Text { payload: text, .. } => {
                s.write(text.as_bytes())?;
            }

            Frame::Binary { payload: bin, .. } => {
                s.write(&bin)?;
            }

            Frame::Close { .. } => {
                return Ok(());
            }

            Frame::Ping { .. } => {}
            Frame::Pong { .. } => {}
        }
    }
}


async fn handle_connection(s: TcpStream, ws_url: &str) -> Res {
    println!("New connection from {:#?}", s.peer_addr()?);

    // Split read and write part of Tcp connection
    let second_stream = s.try_clone()?;

    // Start a new WebSocket connection
    let ws = WebSocket::builder()
        .add_subprotocol("binary")
        .add_header("Origin", "http://solar_system/")
        .add_header("User-Agent", "WSClient/1.0")
        .connect(ws_url).await?;
    let (read, write) = ws.split();

    std::thread::spawn(move || {
        if let Err(e) = tokio::runtime::Runtime::new().unwrap().block_on(handle_write_half(second_stream, write)) {
            eprintln!("Write end error {}", e);
        }
    });

    handle_read_half(s, read).await?;

    Ok(())
}

fn main() {
    let args = std::env::args().collect::<Vec<String>>();

    if args.len() != 4 {
        eprintln!("Usage: {} [listen-addr] [listen-port] [target-ws-url]", args[0]);
        std::process::exit(1);
    }

    let listen_addr = format!("{}:{}", args[1], args[2]);
    let ws_addr = args[3].to_string();
    println!("Listening on {} / Redirecting to {}", listen_addr, &ws_addr);

    let server = TcpListener::bind(listen_addr)
        .expect("Failed to start HTTP server!");


    for s in server.incoming() {
        let s = s.expect("Could not handle a connection!");
        let ws_addr = ws_addr.to_string();

        std::thread::spawn(|| {
            tokio::runtime::Runtime::new().unwrap().block_on(async move {
                let ws_addr = ws_addr.to_string();

                if let Err(e) = handle_connection(s, &ws_addr).await {
                    println!("Could not handle a connection correctly! {}", e);
                }
            });
        });
    };
}
